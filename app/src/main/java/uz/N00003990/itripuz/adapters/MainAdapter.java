package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.MainHolder;
import uz.N00003990.itripuz.models.MainModelData;

public class MainAdapter extends RecyclerView.Adapter<MainHolder> {
    private List<MainModelData> modelList;


    public MainAdapter(List<MainModelData> data) {
        this.modelList = data;
    }

    @Override
    public int getItemViewType(int position) {
        return modelList.get(position).type();
    }

    @NonNull
    @Override
    public MainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0){
      return new MainHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,parent,false));
        }
        else return new MainHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recently_viewed,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MainHolder holder, int position) {
        holder.bindData(modelList.get(position));
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
