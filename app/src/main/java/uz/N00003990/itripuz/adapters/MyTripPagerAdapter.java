package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.io.Serializable;
import java.util.ArrayList;

import uz.N00003990.itripuz.fragments.TripFragment;

public class MyTripPagerAdapter extends FragmentPagerAdapter implements Serializable {
    private ArrayList<String> keys;


    public MyTripPagerAdapter(FragmentManager fm) {
        super(fm);
        loadKeys();

    }

    private void loadKeys(){
        keys = new ArrayList<>();
        keys.add("FAVORITE");
        keys.add("RECENTLY VIEWED");
    }

    @Override
    public Fragment getItem(int position) {
        return TripFragment.getInstance(keys.get(position),this);
    }

    @Override
    public int getCount() {
        return keys.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return keys.get(position);
    }


    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_UNCHANGED;
    }
}
