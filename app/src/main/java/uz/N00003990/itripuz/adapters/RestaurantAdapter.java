package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.RestaurantHolder;
import uz.N00003990.itripuz.models.RestaurantData;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantHolder> {
    private List<RestaurantData> restaurantData;
    private RestaurantHolder.OnClickItemListener onClickItemListener;

    public void setOnClickItemListener(RestaurantHolder.OnClickItemListener onClickItemListener) {
        this.onClickItemListener = onClickItemListener;
    }

    public RestaurantAdapter(List<RestaurantData> restaurantData) {
        this.restaurantData = restaurantData;
    }

    @NonNull
    @Override
    public RestaurantHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RestaurantHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurant,parent,false),onClickItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantHolder holder, int position) {
        holder.bindData(restaurantData.get(position));
    }

    @Override
    public int getItemCount() {
        return restaurantData.size();
    }
}
