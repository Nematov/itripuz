package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.RecentlyDataHolder;
import uz.N00003990.itripuz.models.RecentlyViewedData;

public class RecentlyDataAdapter extends RecyclerView.Adapter<RecentlyDataHolder> {

    private RecentlyDataHolder.OnClickItem onClickFavoriteItem;

    private List<RecentlyViewedData> recentlyViewedData;

    public RecentlyDataAdapter(List<RecentlyViewedData> recentlyViewedData) {
        this.recentlyViewedData = recentlyViewedData;
    }

    public void setOnClickFavoriteItem(RecentlyDataHolder.OnClickItem onClickFavoriteItem) {
        this.onClickFavoriteItem = onClickFavoriteItem;
    }

    @NonNull
    @Override
    public RecentlyDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecentlyDataHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recently_see_all, parent, false), onClickFavoriteItem);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentlyDataHolder holder, int position) {
        holder.bindData(recentlyViewedData.get(position));
    }

    @Override
    public int getItemCount() {
        return recentlyViewedData.size();
    }
}
