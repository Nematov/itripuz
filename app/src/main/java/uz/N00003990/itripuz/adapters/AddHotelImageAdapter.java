package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;

public class AddHotelImageAdapter extends RecyclerView.Adapter<AddHotelImageAdapter.ViewHolder> {
    private ArrayList<String> strings;

    public AddHotelImageAdapter(ArrayList<String> strings) {
        this.strings = strings;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(strings.get(position));
    }

    @Override
    public int getItemCount() {
        return strings == null ? 0 : strings.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
        }

        public void bindData(String imageUrl) {
            Picasso.get().load(imageUrl).into(imageView);
        }
    }
}
