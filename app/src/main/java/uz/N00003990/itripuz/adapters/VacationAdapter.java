package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.VacationHolder;
import uz.N00003990.itripuz.models.VacationData;

public class VacationAdapter extends RecyclerView.Adapter<VacationHolder> {
    private VacationHolder.OnClickItemListener onClickItemListener;
    private ArrayList<VacationData> vacationData;

    public VacationAdapter(ArrayList<VacationData> vacationData) {
        this.vacationData = vacationData;
    }

    public void setOnClickItemListener(VacationHolder.OnClickItemListener onClickItemListener) {
        this.onClickItemListener = onClickItemListener;
    }

    @NonNull
    @Override
    public VacationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VacationHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurant, parent, false), onClickItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull VacationHolder holder, int position) {
        holder.bindData(vacationData.get(position));
    }

    @Override
    public int getItemCount() {
        return vacationData == null ? 0 : vacationData.size();
    }
}
