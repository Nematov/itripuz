package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.HotelHolder;
import uz.N00003990.itripuz.models.HotelData;

public class HotelAdapter extends RecyclerView.Adapter<HotelHolder> {

    private List<HotelData> hotels;
    private HotelHolder.OnClickItemHotelListener onClickItemHotelListener;

    public void setOnClickItemHotelListener(HotelHolder.OnClickItemHotelListener onClickItemHotelListener) {
        this.onClickItemHotelListener = onClickItemHotelListener;
    }

    public HotelAdapter(List<HotelData> hotels) {
        this.hotels = hotels;
    }

    @NonNull
    @Override
    public HotelHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HotelHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hotel, parent, false), onClickItemHotelListener);
    }

    @Override
    public void onBindViewHolder(@NonNull HotelHolder holder, int position) {
        holder.bindData(hotels.get(position));
    }

    @Override
    public int getItemCount() {
        return hotels == null ? 0 : hotels.size();
    }

    public void setData(List<HotelData> data) {
        this.hotels = data;
    }
}
