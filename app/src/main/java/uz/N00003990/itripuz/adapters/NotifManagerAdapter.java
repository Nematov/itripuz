package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.NotifManagerHolder;
import uz.N00003990.itripuz.models.NotificationModel;

public class NotifManagerAdapter extends RecyclerView.Adapter<NotifManagerHolder> {
    private ArrayList<NotificationModel> notificationModels;
    private NotifManagerHolder.OnClickNotifDataListener onClickNotifDataListener;

    public NotifManagerAdapter(ArrayList<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
    }

    public void setOnClickNotifDataListener(NotifManagerHolder.OnClickNotifDataListener onClickNotifDataListener) {
        this.onClickNotifDataListener = onClickNotifDataListener;
    }

    @NonNull
    @Override
    public NotifManagerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotifManagerHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_manager_layout, parent, false), onClickNotifDataListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NotifManagerHolder holder, int position) {
        holder.bindData(notificationModels.get(position));
    }

    @Override
    public int getItemCount() {
        return notificationModels == null ? 0 : notificationModels.size();
    }
}
