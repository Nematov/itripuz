package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.ArrayList;
import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.RoomHolder;
import uz.N00003990.itripuz.models.RoomData;

public class RoomAdapter extends RecyclerView.Adapter<RoomHolder> {
    private ArrayList<RoomData> roomData;
    private RoomHolder.OnClickRoomItemListener onClickRoomItemListener;

    public void setOnClickRoomItemListener(RoomHolder.OnClickRoomItemListener onClickRoomItemListener) {
        this.onClickRoomItemListener = onClickRoomItemListener;
    }

    public RoomAdapter(ArrayList<RoomData> roomData) {
        this.roomData = roomData;
    }

    @NonNull
    @Override
    public RoomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RoomHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_room, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RoomHolder holder, int position) {
        holder.bindData(roomData.get(position));
        holder.setOnClickRoomItemListener(onClickRoomItemListener);
    }

    @Override
    public int getItemCount() {
        return roomData.size();
    }
}
