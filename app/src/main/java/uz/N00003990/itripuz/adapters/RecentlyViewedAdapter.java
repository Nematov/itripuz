package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.RecentlyViewedHolder;
import uz.N00003990.itripuz.models.RecentlyViewedData;

public class RecentlyViewedAdapter extends RecyclerView.Adapter<RecentlyViewedHolder> {

    private List<RecentlyViewedData> viewedData;

    public RecentlyViewedAdapter(List<RecentlyViewedData> viewedData) {
        this.viewedData = viewedData;
    }

    @NonNull
    @Override
    public RecentlyViewedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecentlyViewedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recently_viewed, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecentlyViewedHolder holder, int position) {
        holder.bindData(viewedData.get(position));
    }

    @Override
    public int getItemCount() {
        return viewedData != null ? viewedData.size() : 0;
    }
}
