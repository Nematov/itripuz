package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.BusyHolder;
import uz.N00003990.itripuz.models.BusyData;

public class BusyAdapter extends RecyclerView.Adapter<BusyHolder> {
    private ArrayList<BusyData> busyData;

    public BusyAdapter(ArrayList<BusyData> busyData) {
        this.busyData = busyData;
    }

    @NonNull
    @Override
    public BusyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BusyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_busy, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BusyHolder holder, int position) {
        holder.bindData(busyData.get(position));
    }

    @Override
    public int getItemCount() {
        return busyData == null ? 0 : busyData.size();
    }
}
