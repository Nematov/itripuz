package uz.N00003990.itripuz.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.CitiesData;

public class ViewGroupPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> images;
    private FirebaseDatabase database;
    private DatabaseReference reference;

    public ViewGroupPagerAdapter(Context context, ArrayList<String> images) {
        this.context = context;
        this.images = images;
        database = FirebaseDatabase.getInstance();

    }

    @Override
    public Object instantiateItem(ViewGroup parent, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_pager, null, false);
        ImageView imageView = view.findViewById(R.id.pager_img);
        String key = images.get(position);
        reference = database.getReference("cities").child(key);
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("NewApi")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                CitiesData citiesData = dataSnapshot.getValue(CitiesData.class);
                Picasso.get().load(citiesData.getImgUrl())
                        .centerCrop()
                        .fit()
                        .error(R.drawable.ic_error_black_24dp)
                        .into(imageView);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        parent.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup parent, int position, Object object) {
        View view = (View) object;
        parent.removeView(view);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}