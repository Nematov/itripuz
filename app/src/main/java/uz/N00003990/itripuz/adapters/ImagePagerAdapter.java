package uz.N00003990.itripuz.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import uz.N00003990.itripuz.fragments.ImagePagerFragment;

public class ImagePagerAdapter extends FragmentPagerAdapter {
    private ArrayList<String> list;

    public ImagePagerAdapter(FragmentManager fm, ArrayList<String> list) {
        super(fm);
        this.list=list;
    }

    @Override
    public Fragment getItem(int position) {
        return ImagePagerFragment.getInstance(list.get(position));
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
