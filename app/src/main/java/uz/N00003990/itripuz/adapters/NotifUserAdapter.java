package uz.N00003990.itripuz.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.holders.NotifUserHolder;
import uz.N00003990.itripuz.models.NotificationModel;

public class NotifUserAdapter extends RecyclerView.Adapter<NotifUserHolder> {
    private ArrayList<NotificationModel> notificationModels;

    @NonNull
    @Override
    public NotifUserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotifUserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_user_layout, parent, false));
    }

    public NotifUserAdapter(ArrayList<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
    }

    @Override
    public void onBindViewHolder(@NonNull NotifUserHolder holder, int position) {
        holder.bindData(notificationModels.get(position));
    }

    @Override
    public int getItemCount() {
        return notificationModels == null ? 0 : notificationModels.size();
    }
}
