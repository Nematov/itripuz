package uz.N00003990.itripuz;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.rd.PageIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.adapters.RecentlyViewedAdapter;
import uz.N00003990.itripuz.adapters.ViewGroupPagerAdapter;
import uz.N00003990.itripuz.dialogs.LoadingDialog;
import uz.N00003990.itripuz.fragments.MyTripFragment;
import uz.N00003990.itripuz.fragments.ProfileFragment;
import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.mvp.presenter_implement.ScrollingActivityPresenterImple;
import uz.N00003990.itripuz.mvp.views.ScrollingActivityView;
import uz.N00003990.itripuz.services.JobService;
import uz.N00003990.itripuz.settings.Settings;

public class ScrollingActivity extends AppCompatActivity implements ScrollingActivityView {
    private ScrollingActivityPresenterImple presenter;

    private AppBarLayout appBarLayout;

    private ViewGroupPagerAdapter pagerAdapter;
    private String cityKey;
    private ViewPager pager;
    private RecentlyViewedAdapter recentlyViewedAdapter;
    private RecyclerView list;
    private TextView toolbarText;
    private ArrayList<String> citiesList;
    private List<RecentlyViewedData> recentlyViewedData;
    private BottomNavigationView navigationView;
    private FragmentManager manager;
    private Toolbar toolbar;
    private PageIndicatorView indicator;
    private LoadingDialog dialog;
    private TextView lastDataText;
    private TextView seeAllText;
    private JobScheduler jobScheduler;
    private final int LOAD_ARTWORK_JOB_ID = 100;

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            jobScheduler.cancel(LOAD_ARTWORK_JOB_ID);
//        }
    }

    @SuppressLint("CommitTransaction")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        findViewById();
        loadView();
        loadData();
        eventElementListener();
        EventBus.getDefault().register(this);
        hideStatusBar();
        FirebaseMessaging.getInstance().subscribeToTopic(FirebaseAuth.getInstance().getCurrentUser().getUid());


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            jobScheduler.schedule(new JobInfo.Builder(LOAD_ARTWORK_JOB_ID,
//                    new ComponentName(this, JobService.class))
//                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                    .build());
//        }

//        startService(new Intent(this,CheckDatabaseService.class));
    }

    private void showStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.TYPE_STATUS_BAR);
        }
    }

    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.TYPE_STATUS_BAR);
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void finish(String s) {
        if (s.equals("s"))
            finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        EventBus.getDefault().unregister(this);
    }

    private void findViewById() {
        toolbar = findViewById(R.id.toolbar);
        pager = findViewById(R.id.pager);
        list = findViewById(R.id.list);
        lastDataText = findViewById(R.id.last_data_text);
        seeAllText = findViewById(R.id.see_all);
        seeAllText.setVisibility(View.INVISIBLE);
        lastDataText.setVisibility(View.INVISIBLE);
        navigationView = findViewById(R.id.bottom_navigation);
        toolbarText = findViewById(R.id.toolbar_text);
        appBarLayout = findViewById(R.id.app_bar);
        indicator = findViewById(R.id.pageIndicatorView);
        appBarLayout = findViewById(R.id.app_bar);
    }

    private void loadView() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
//        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle("");
        indicator.setVisibility(View.INVISIBLE);
        indicator.setRadius(8f);
    }

    private void loadData() {
        citiesList = new ArrayList<>();
        dialog = new LoadingDialog(this);
        presenter = new ScrollingActivityPresenterImple(this);
        presenter.loadData();
        presenter.loadRecentlyViewedData();
    }

    private void eventElementListener() {

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                cityKey = citiesList.get(position);
                toolbarText.setText(cityKey);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        indicator.setViewPager(pager);
        navigationView.setOnNavigationItemSelectedListener(item -> {
            FragmentTransaction transaction;
            manager = getSupportFragmentManager();
            transaction = manager.beginTransaction();
            if (item.getItemId() == R.id.bottom_navigation_profile) {
//                transaction.replace(R.id.layout, new ProfileFragment(), "profile");
//                transaction.addToBackStack(null);
//                transaction.commit();
                showStatusBar();
                startActivity(new Intent(this, AdminActivity.class));
            }
            if (item.getItemId() == R.id.bottom_navigation_home) {
                int count = getSupportFragmentManager().getBackStackEntryCount();
                while (count-- > 0) {
                    getSupportFragmentManager().popBackStack();
                }
                hideStatusBar();
            }
            if (item.getItemId() == R.id.bottom_navigation_trip) {
                if (manager.findFragmentByTag("TRIP") == null || manager.findFragmentByTag("profile").isVisible()) {
                    transaction.replace(R.id.layout, new MyTripFragment());
                    transaction.addToBackStack(null);
                    transaction.commit();
                    showStatusBar();
                }
            }
            return true;
        });

        navigationView.setOnNavigationItemReselectedListener(item -> {
                    return;
                }
        );

        seeAllText.setOnClickListener(v -> {
            navigationView.setSelectedItemId(R.id.bottom_navigation_trip);
            manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.layout, MyTripFragment.getInstance(true), "TRIP");
            transaction.addToBackStack(null);
            transaction.commit();
            showStatusBar();
//            navigationView.setSelectedItemId(R.id.bottom_navigation_trip);
        });
    }

    @Override

    public void onBackPressed() {
        if (manager.findFragmentByTag("info") != null) {
            manager.popBackStack();
        } else {
            navigationView.setSelectedItemId(R.id.bottom_navigation_home);
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else
                super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            manager.popBackStack();
        }
        return true;
    }


    public void onClickHotel(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        Settings.getSettings().setKey("KEY", "hotel");
        startActivity(intent);
    }

    public void onClickVacation(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        Settings.getSettings().setKey("KEY", "vacation");
        startActivity(intent);
    }

    public void onClickTaxi(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=ru.yandex.taxi&hl=en"));
        startActivity(intent);
    }

    public void onClickRestaurant(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        Settings.getSettings().setKey("KEY", "restaurant");
        startActivity(intent);
    }

    @Override
    public void setDataToView(ArrayList<String> citiesList) {
        this.citiesList = citiesList;
        if (pagerAdapter == null) {
            indicator.setCount(citiesList.size());
            pagerAdapter = new ViewGroupPagerAdapter(ScrollingActivity.this, citiesList);
            pager.setAdapter(pagerAdapter);
            cityKey = citiesList.get(0);
            toolbarText.setText(cityKey);
            indicator.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setRecentlyDataToView(List<RecentlyViewedData> recentlyDataToView) {
        this.recentlyViewedData = recentlyDataToView;
        if (recentlyDataToView.size() > 0) {
            lastDataText.setVisibility(View.VISIBLE);
            seeAllText.setVisibility(View.VISIBLE);
        }
        recentlyViewedAdapter = new RecentlyViewedAdapter(recentlyViewedData);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        list.setAdapter(recentlyViewedAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void notifyRecentlyData(RecentlyViewedData data) {
        presenter.loadRecentlyViewedData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void selectHome(String s) {
        navigationView.setSelectedItemId(R.id.bottom_navigation_home);
    }

    @Override
    public void showLoading() {
        dialog.show();
    }

    @Override
    public void hideLoading() {
        dialog.cancel();
    }
}
