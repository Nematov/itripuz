package uz.N00003990.itripuz.app;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import uz.N00003990.itripuz.room.database.Database;
import uz.N00003990.itripuz.settings.Settings;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Settings.init(getApplicationContext());
        Database.init(getApplicationContext());
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//        if (FirebaseAuth.getInstance().getCurrentUser() != null)
//        FirebaseMessaging.getInstance().subscribeToTopic(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
