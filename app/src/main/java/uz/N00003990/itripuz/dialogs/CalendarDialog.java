package uz.N00003990.itripuz.dialogs;

import android.content.Context;
import android.widget.CalendarView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import uz.N00003990.itripuz.R;

public class CalendarDialog extends Dialog {
    private CalendarView calendarView;
    private String year;
    private String month;
    private String dayOfMonth;
    private String selectedDay;
    private long selectedDayTime;

    public CalendarDialog(Context context) {
        super(context, R.layout.dialog_calendar);
        calendarView = findViewById(R.id.calendar);
        selectedDay = getDate();
        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            String d = dayOfMonth < 10 ? ("0" + dayOfMonth) : String.valueOf(dayOfMonth);
            int i = month + 1;
            String m = i < 10 ? ("0" + i) : String.valueOf(i);
            selectedDay = d + "-" + m + "-" + String.valueOf(year);
        });

    }

    public long converTimeSeconds(String d) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = simpleDateFormat.parse(d);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String convertDateString(long l){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(l);
        return simpleDateFormat.format(date);
    }

    public String getSelectDate() {
        return selectedDay;
    }

    public String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(calendarView.getDate());
        String s = simpleDateFormat.format(date);
        return s;
    }


}
