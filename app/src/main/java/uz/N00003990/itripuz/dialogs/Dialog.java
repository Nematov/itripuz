package uz.N00003990.itripuz.dialogs;


import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by User on 03.02.2018.
 */

public class Dialog {
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private View view;
    private OnClickListener onClickListener;

    public Dialog(Context context,int resId) {
        Context context1;
        builder = new AlertDialog.Builder(context);
        view = LayoutInflater.from(context).inflate(resId,null,false);
        builder.setView(view);
        builder.setPositiveButton("Ok",(dialogInterface, i) -> {
            if (onClickListener != null){
                onClickListener.Apply();
            }
        });
        builder.setNegativeButton("Cancel",(dialogInterface, i) -> {
            if (onClickListener != null){
                onClickListener.Cancel();
            }
        });
        dialog = builder.create();
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    protected void setTitle(String title){
        builder.setTitle(title);
    }

    protected <T extends View> T findViewById(int id){
        return view.findViewById(id);
    }

    public void show(){
       dialog.show();
    }

    public void hide(){
        dialog.hide();
    }

    public interface OnClickListener{
        void Apply();
        void Cancel();
    }

}
