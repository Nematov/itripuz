package uz.N00003990.itripuz.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.BusyData;

public class BusyDialog extends Dialog {
    private EditText name;
    private EditText phone;
    private EditText passport;
    private EditText day;
    private TextView total;
    private TextView cancel;
    private TextView ok;
    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public BusyDialog(@NonNull Context context) {
        super(context);
    }

    protected BusyDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_busy);

//        if (getWindow() != null)
//            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        setCanceledOnTouchOutside(false);
//        setCancelable(false);
        name = findViewById(R.id.name);
        passport = findViewById(R.id.passport);
        phone = findViewById(R.id.phone_number);
        cancel = findViewById(R.id.cancel);
        day = findViewById(R.id.day);
        total = findViewById(R.id.total_cost);
        ok = findViewById(R.id.ok);
        cancel.setOnClickListener(v -> {
            if (onClickListener != null) {
                onClickListener.Cancel();
            }
        });
        ok.setOnClickListener(v -> {
            if (onClickListener != null) onClickListener.Apply();
        });
        day.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (onClickListener != null) onClickListener.changeDayTextListener();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public boolean isEmptyFields() {
        if (TextUtils.isEmpty(name.getText())) name.setError("write name");
        if (TextUtils.isEmpty(phone.getText())) phone.setError("write phone number");
        if (TextUtils.isEmpty(passport.getText())) passport.setError("write passport series");
        if (TextUtils.isEmpty(day.getText())) day.setError("write day");

        return !TextUtils.isEmpty(day.getText()) && !TextUtils.isEmpty(name.getText()) && !TextUtils.isEmpty(phone.getText()) && !TextUtils.isEmpty(passport.getText());
    }

    public void setTotal(String total) {
        this.total.setText(total);
    }

    public String getName() {
        return name.getText().toString();
    }

    public String getPhone() {
        return phone.getText().toString();
    }

    public String getPassport() {
        return passport.getText().toString();
    }

    public int getDay() {
        return TextUtils.isEmpty(day.getText()) ? 0 : Integer.parseInt(day.getText().toString());
    }

    public interface OnClickListener {
        void Apply();

        void Cancel();

        void changeDayTextListener();
    }
}
