package uz.N00003990.itripuz;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Objects;

import uz.N00003990.itripuz.adapters.HotelAdapter;
import uz.N00003990.itripuz.fragments.AddHotelFragment;
import uz.N00003990.itripuz.fragments.HotelRoomFragment;
import uz.N00003990.itripuz.holders.HotelHolder;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.HotelList;
import uz.N00003990.itripuz.mvp.presenter_implement.AdminActivityPresenterImpl;
import uz.N00003990.itripuz.mvp.views.AdminActivityView;
import uz.N00003990.itripuz.settings.Settings;

public class AdminActivity extends AppCompatActivity implements AdminActivityView, HotelHolder.OnClickItemHotelListener {
    private AdminActivityPresenterImpl presenter;

    private FloatingActionButton floatingActionButton;
    private DatabaseReference reference;
    private DatabaseReference databaseReference;
    private Query query;
    private ArrayList<HotelData> hotelData;
    private HotelAdapter hotelAdapter;
    private RecyclerView recyclerView;
    private FirebaseUser user;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("My Hotels");
        floatingActionButton = findViewById(R.id.add_button);
        recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        user = FirebaseAuth.getInstance().getCurrentUser();
        presenter = new AdminActivityPresenterImpl(this);
        presenter.getMyHotelList(user.getUid());
        Settings.getSettings().setKey("admin", true);
        floatingActionButton.setOnClickListener(v -> {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, new AddHotelFragment());
            transaction.addToBackStack(null);
            transaction.commit();
//            floatingActionButton.setVisibility(View.INVISIBLE);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().post("d");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.TYPE_STATUS_BAR);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.log_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.log_out){
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this,SignInActivity.class));
            finish();
            EventBus.getDefault().post("s");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSetMyHotelData(ArrayList<HotelData> hotelData) {
        this.hotelData = hotelData;
        hotelAdapter = new HotelAdapter(this.hotelData);
        hotelAdapter.setOnClickItemHotelListener(this);
        recyclerView.setAdapter(hotelAdapter);
    }

    @Override
    public void onSuccessEdit() {

    }

    @Override
    public void onSuccessDelete() {
        hotelAdapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorEdit() {

    }

    @Override
    public void onErrorDelete() {

    }

    @Override
    public void onClickItem(int position, HotelData data) {

    }

    @Override
    public void onClickBusy(int position, HotelData data) {

    }

    @Override
    public void onClickViewItem(int position, HotelData data) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, HotelRoomFragment.getInstance(data));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onClickDelete(int position, HotelData data) {
        presenter.deleteMyHotelData(data);
        hotelData.remove(position);
    }
}
