package uz.N00003990.itripuz.retrofit.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMessageData {
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("who_for")
    @Expose
    private String whoFor;
    @SerializedName("answer")
    @Expose
    private String answer;

    public UserMessageData() {
    }

    public UserMessageData(String topic, String whoFor, String answer) {
        this.topic = topic;
        this.whoFor = whoFor;
        this.answer = answer;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getWhoFor() {
        return whoFor;
    }

    public void setWhoFor(String whoFor) {
        this.whoFor = whoFor;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

}
