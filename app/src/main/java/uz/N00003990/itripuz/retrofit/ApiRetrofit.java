package uz.N00003990.itripuz.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiRetrofit {
    private static final String BASE_URL = "https://gcm-http.googleapis.com/gcm/";
    private static String apiKey = "AAAA7IMgo_Y:APA91bH036aYuw4E1sk55r-FVMVMo4ZTOoNxhI6fhIFONHMyIJ0zVx7CXqCy9oSvmA5ifMxIParO8cug9M9jeIJHJqS0n8QIeE7sNdoFdGK5_jwW_RkTBerH46A9Wwq9jxdiqVy5UNlF";
    private static Retrofit retrofit;

    private static OkHttpClient getHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder();
            requestBuilder.addHeader("Content-Type", "application/json");
            requestBuilder.addHeader("Authorization", "key=" + apiKey);
            Request request = requestBuilder.build();
            return chain.proceed(request);
        });
        return httpClient.build();
    }


    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
