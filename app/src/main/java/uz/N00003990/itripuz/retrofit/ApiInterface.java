package uz.N00003990.itripuz.retrofit;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import uz.N00003990.itripuz.retrofit.models.ResponseData;

public interface ApiInterface {
    @POST("send")
    Call<ResponseData> sendMessage(@Body JsonObject JsonObject);
}
