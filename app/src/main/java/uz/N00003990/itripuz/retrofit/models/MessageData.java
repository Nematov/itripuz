package uz.N00003990.itripuz.retrofit.models;

import uz.N00003990.itripuz.models.NotificationModel;

public class MessageData {
    private String to;

    private NotificationModel data;

    public MessageData() {
    }

    public void setTopic(String topic) {
        this.to = topic;
    }


    public void setNotificationModel(NotificationModel notificationModel) {
        this.data = notificationModel;
    }

    public String getTopic() {
        return to;
    }


    public NotificationModel getNotificationModel() {
        return data;
    }

    public MessageData(String topic,  NotificationModel notificationModel) {
        this.to = topic;
        this.data = notificationModel;
    }
}
