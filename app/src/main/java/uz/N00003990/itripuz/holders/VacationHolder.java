package uz.N00003990.itripuz.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.custom_ui.CustomRatingBar;
import uz.N00003990.itripuz.models.VacationData;

public class VacationHolder extends RecyclerView.ViewHolder {
    private VacationData data;
    private ImageView vacationImg;
    private TextView vacationName;
    private TextView vacationtReview;
    private TextView sub;
    private CustomRatingBar ratingBar;
    private Context context;
    private OnClickItemListener onClickItemListener;

    public VacationHolder(View itemView, OnClickItemListener onClickItemListener) {
        super(itemView);
        this.onClickItemListener = onClickItemListener;
        context = itemView.getContext();
        ratingBar = itemView.findViewById(R.id.restaurant_rating);
        vacationImg = itemView.findViewById(R.id.item_restaurant_img);
        vacationName = itemView.findViewById(R.id.item_restaurant_name);
        vacationtReview = itemView.findViewById(R.id.item_restaurant_reviews);
        sub = itemView.findViewById(R.id.item_restaurant_sub);
        itemView.setOnClickListener(v -> {
            if (onClickItemListener != null) {
                onClickItemListener.onClickItem(data);
            }
        });
    }

    public void bindData(VacationData vacationData) {
        data = vacationData;
        vacationName.setText(vacationData.getName());
        vacationtReview.setText("" + vacationData.getReview());
        sub.setText(vacationData.getAddress());
        Picasso.get().load(vacationData.getImgUrl()).into(vacationImg);
    }

    public interface OnClickItemListener {
        void onClickItem(VacationData data);
    }
}
