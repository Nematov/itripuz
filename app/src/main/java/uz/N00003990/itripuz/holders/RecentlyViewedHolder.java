package uz.N00003990.itripuz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.RecentlyViewedData;

public class RecentlyViewedHolder extends RecyclerView.ViewHolder{
    private RecentlyViewedData viewedData;
    private ImageView imageView;
    private TextView textView;

    public RecentlyViewedHolder(View itemView) {
        super(itemView);
    imageView = itemView.findViewById(R.id.item_recently_img);
    textView = itemView.findViewById(R.id.item_recently_text);
    }

    public void bindData(RecentlyViewedData recentlyViewedData) {
    this.viewedData = recentlyViewedData;
        Picasso.get()
                .load(recentlyViewedData.getImgUrl())
                .error(R.drawable.ic_error_black_24dp)
                .centerCrop()
                .fit()
                .into(imageView);

        textView.setText(recentlyViewedData.getName());
    }
}
