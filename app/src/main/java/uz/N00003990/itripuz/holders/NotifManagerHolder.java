package uz.N00003990.itripuz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.NotificationModel;

public class NotifManagerHolder extends RecyclerView.ViewHolder {
    private TextView hotelName;
    private TextView userName;
    private TextView userNumber;
    private TextView date;
    private TextView cancel;
    private TextView ok;
    private NotificationModel notificationModel;
    private OnClickNotifDataListener onClickNotifDataListener;

    public NotifManagerHolder(View itemView, OnClickNotifDataListener onClickNotifDataListener) {
        super(itemView);
        hotelName = itemView.findViewById(R.id.notif_item_name);
        userName = itemView.findViewById(R.id.notif_name_user);
        userNumber = itemView.findViewById(R.id.notif_number_user);
        date = itemView.findViewById(R.id.notif_date_and_time);
        cancel = itemView.findViewById(R.id.notif_cancel);
        ok = itemView.findViewById(R.id.notif_accept);
        ok.setOnClickListener(v -> {
            if (onClickNotifDataListener != null) {
                onClickNotifDataListener.onClickOk(getAdapterPosition(), notificationModel);
            }
        });
        cancel.setOnClickListener(v -> {
            if (onClickNotifDataListener != null) {
                onClickNotifDataListener.onClickCancel(getAdapterPosition(), notificationModel);
            }
        });
    }

    public void bindData(NotificationModel notificationModel) {
        this.notificationModel = notificationModel;
        hotelName.setText(notificationModel.getHotelName());
        userNumber.setText(notificationModel.getBusyData().get(0).getPhoneNumber());
        userName.setText(notificationModel.getBusyData().get(0).getName());
        date.setText(notificationModel.getBusyData().get(0).getDate());
    }

    public interface OnClickNotifDataListener {
        void onClickOk(int p, NotificationModel notificationModel);

        void onClickCancel(int p, NotificationModel notificationModel);
    }
}
