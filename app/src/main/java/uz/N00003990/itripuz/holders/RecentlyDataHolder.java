package uz.N00003990.itripuz.holders;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.custom_ui.CustomRatingBar;
import uz.N00003990.itripuz.models.RecentlyViewedData;

public class RecentlyDataHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private TextView review;
    private TextView sub;
    private CustomRatingBar ratingBar;
    private ImageView favorite;
    private ImageView img;
    private OnClickItem onClickFavoriteItem;
    private Context context;
    private RecentlyViewedData data;


    public RecentlyDataHolder(View itemView, OnClickItem onClickFavoriteItem) {
        super(itemView);
        context = itemView.getContext();
        this.onClickFavoriteItem = onClickFavoriteItem;
        findViewById(itemView);
        itemView.setOnClickListener(v -> {
            if (onClickFavoriteItem != null) {
                onClickFavoriteItem.onClickItem(data);
            }
        });
        favorite.setOnClickListener(v -> {
            if (onClickFavoriteItem != null) {
                onClickFavoriteItem.onClickFavorite(data, getAdapterPosition());
            }
        });
    }

    private void findViewById(View itemView) {
        name = itemView.findViewById(R.id.item_recently_name);
        review = itemView.findViewById(R.id.item_recently_reviews);
        ratingBar = itemView.findViewById(R.id.recently_rating);
        favorite = itemView.findViewById(R.id.recently_favorite);
        sub = itemView.findViewById(R.id.item_recently_sub);
        img = itemView.findViewById(R.id.item_recently_img);

    }

    @SuppressLint("SetTextI18n")
    public void bindData(RecentlyViewedData data) {
        this.data = data;
        name.setText(data.getName());
        review.setText(data.getReviews() + "Reviews");
        ratingBar.setRating(5, data.getRating());
        ratingBar.setColor(ContextCompat.getColor(context, R.color.colorRating));
        sub.setText("type: " + data.getType());
        if (data.getFavorite() == 1) {
            favorite.setVisibility(View.VISIBLE);
            favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
        }else {
            favorite.setVisibility(View.GONE);
        }

        Picasso.get()
                .load(data.getImgUrl())
                .centerCrop()
                .fit()
                .into(img);
    }

    public interface OnClickItem {
        void onClickFavorite(RecentlyViewedData data, int position);

        void onClickItem(RecentlyViewedData data);
    }
}
