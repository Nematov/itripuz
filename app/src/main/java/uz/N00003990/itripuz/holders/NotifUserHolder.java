package uz.N00003990.itripuz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.NotificationModel;

public class NotifUserHolder extends RecyclerView.ViewHolder {
    private TextView hotelName;
    private TextView date;
    private TextView answer;

    public NotifUserHolder(View itemView) {
        super(itemView);
        hotelName = itemView.findViewById(R.id.notif_item_name_user);
        date = itemView.findViewById(R.id.notif_date_and_time_table_user);
        answer = itemView.findViewById(R.id.notif_answer_user);
    }

    public void bindData(NotificationModel notificationModel){
        hotelName.setText(notificationModel.getHotelName());
        date.setText(notificationModel.getBusyData().get(0).getDate());
        answer.setText(notificationModel.getManagerAnswered());
    }
}
