package uz.N00003990.itripuz.holders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.custom_ui.CustomRatingBar;
import uz.N00003990.itripuz.models.RestaurantData;

public class RestaurantHolder extends RecyclerView.ViewHolder {
    private RestaurantData data;
    private ImageView restaurantImg;
    private TextView restaurantName;
    private TextView restaurantReview;
    private CustomRatingBar ratingBar;
    private Context  context;
    private OnClickItemListener onClickItemListener;

    public RestaurantHolder(View itemView,OnClickItemListener onClickItemListener) {
        super(itemView);
        this.onClickItemListener = onClickItemListener;
        context = itemView.getContext();
        ratingBar = itemView.findViewById(R.id.restaurant_rating);
        restaurantImg = itemView.findViewById(R.id.item_restaurant_img);
        restaurantName = itemView.findViewById(R.id.item_restaurant_name);
        restaurantReview = itemView.findViewById(R.id.item_restaurant_reviews);
        itemView.setOnClickListener(v -> {
            if (onClickItemListener != null){
                onClickItemListener.onClickItem(data);
            }
        });
    }


    public void bindData(RestaurantData restaurantData) {
        this.data = restaurantData;
        ratingBar.setRating(5,restaurantData.getRating());
        ratingBar.setColor(ContextCompat.getColor(context, R.color.colorRating));
        restaurantName.setText(restaurantData.getName());
        restaurantReview.setText(restaurantData.getReview()+" Reviews");
        Picasso.get()
                .load(restaurantData.getImgUrl())
                .centerCrop()
                .fit()
                .error(R.drawable.ic_error_black_24dp)
                .into(restaurantImg);
    }

    public interface OnClickItemListener{
        void onClickItem(RestaurantData data);
    }
}
