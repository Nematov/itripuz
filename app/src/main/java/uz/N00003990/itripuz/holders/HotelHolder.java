package uz.N00003990.itripuz.holders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.custom_ui.CustomRatingBar;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.settings.Settings;

public class HotelHolder extends RecyclerView.ViewHolder {
    private HotelData hotel;
    private ImageView hotelImg;
    private ImageView more;
    private ImageView busy;
    private TextView hotelName;
    private TextView hotelReviews;
    private TextView hotelCost;
    private Button button;
    private Context context;
    private CustomRatingBar ratingBar;
    private OnClickItemHotelListener onClickItemHotelListener;


    public HotelHolder(View itemView, OnClickItemHotelListener onClickItemHotelListener) {
        super(itemView);
        this.onClickItemHotelListener = onClickItemHotelListener;
        findId(itemView);
        context = itemView.getContext();
        button.setOnClickListener(view -> {
            if (onClickItemHotelListener != null) {
                onClickItemHotelListener.onClickItem(getAdapterPosition(), hotel);
            }
        });
        itemView.setOnClickListener(v -> {
            if (onClickItemHotelListener != null) {
                onClickItemHotelListener.onClickViewItem(getAdapterPosition(), hotel);
            }
        });
        busy.setOnClickListener(v -> {
            if (onClickItemHotelListener != null){
                onClickItemHotelListener.onClickBusy(getAdapterPosition(),hotel);
            }
        });
        more.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(v.getContext(),v);
            popupMenu.inflate(R.menu.menu_more);
            popupMenu.setOnMenuItemClickListener(item -> {
                if (item.getItemId() == R.id.delete){
                    if (onClickItemHotelListener != null){
                        onClickItemHotelListener.onClickDelete(getAdapterPosition(),hotel);
                    }
                }
            return true;
            });
            popupMenu.show();
        });
    }

    private void findId(View view) {
        hotelImg = view.findViewById(R.id.hotel_img);
        hotelName = view.findViewById(R.id.hotel_name);
        hotelCost = view.findViewById(R.id.hotel_cost);
        hotelReviews = view.findViewById(R.id.hotel_reviews);
        button = view.findViewById(R.id.view_btn);
        ratingBar = view.findViewById(R.id.hotel_rating);
        more = itemView.findViewById(R.id.more);
        busy = itemView.findViewById(R.id.add_busy);
    }

    public void bindData(HotelData hotelData) {
        if (Settings.getSettings().getBoolean("admin")) {
            busy.setVisibility(View.GONE);
            button.setVisibility(View.GONE);
        } else more.setVisibility(View.GONE);
        this.hotel = hotelData;
        hotelName.setText(hotelData.getName());
//        hotelCost.setText(hotelData.getCost()+" UZS");
        hotelReviews.setText(hotelData.getReviews() + " Reviews");
        Picasso.get().
                load(hotelData.getImgUrl().get(0)).error(R.drawable.ic_error_black_24dp)
                .centerCrop()
                .fit()
                .into(hotelImg);
        ratingBar.setRating(5, hotelData.getRating());
        ratingBar.setColor(ContextCompat.getColor(context, R.color.colorRating));
    }

    public interface OnClickItemHotelListener {
        void onClickItem(int position, HotelData data);

        void onClickBusy(int position, HotelData data);

        void onClickViewItem(int position, HotelData data);
        void onClickDelete(int position, HotelData data);
    }
}
