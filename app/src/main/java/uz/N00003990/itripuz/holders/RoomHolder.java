package uz.N00003990.itripuz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.RoomData;

public class RoomHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private TextView cost;
    private ImageView icon;
    private RoomData roomData;
    private OnClickRoomItemListener onClickRoomItemListener;

    public void setOnClickRoomItemListener(OnClickRoomItemListener onClickRoomItemListener) {
        this.onClickRoomItemListener = onClickRoomItemListener;
    }

    public RoomHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.name);
        cost = itemView.findViewById(R.id.cost);
        icon = itemView.findViewById(R.id.icon_busy);
        icon.setOnClickListener(v -> {
            if (onClickRoomItemListener != null)
                onClickRoomItemListener.onClickIcon(roomData);
        });
    }

    public void bindData(RoomData roomData) {
        this.roomData = roomData;
        name.setText(roomData.getName());
        cost.setText(roomData.getCost().toString() + " $");
    }

   public interface OnClickRoomItemListener {
        void onClickIcon(RoomData roomData);
    }

}
