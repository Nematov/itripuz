package uz.N00003990.itripuz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.BusyData;

public class BusyHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private TextView phoneNumber;
    private TextView date;

    public BusyHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.name);
        phoneNumber = itemView.findViewById(R.id.phone_number);
        date = itemView.findViewById(R.id.date);
    }

    public void bindData(BusyData busyData){
        name.setText(busyData.getName());
        phoneNumber.setText(busyData.getPhoneNumber());
        date.setText(busyData.getDate());
    }
}
