package uz.N00003990.itripuz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.MainModelData;

public class MainHolder extends RecyclerView.ViewHolder {
    private MainModelData mainModel;
    private TextView recentlyText;
    private ImageView recentlyView;


    public MainHolder(View itemView ) {
        super(itemView);
//        this.onClickItemListener = onClickItemListener;
        findId(itemView);

    }

    private void findId(View view) {

        recentlyText = view.findViewById(R.id.item_recently_text);
        recentlyView = view.findViewById(R.id.item_recently_img);

    }

    public void bindData(MainModelData mainModel) {
        this.mainModel = mainModel;
//        if (mainModel.type() == 1) {

//            recentlyText.setText(data.getName());
//            Picasso.get().load(data.getImgUrl()).
//                    error(R.drawable.ic_error_black_24dp)
//                    .into(recentlyView);
        }



//    public interface OnClickItemListener {
//        void onClickHotel();
//
//        void onClickVacation();
//
//        void onClickFlights();
//
//        void onClickRestaurant();
//
//        void onClickToDo();
//
//        void onClickForum();
//    }
}
