package uz.N00003990.itripuz.room.database;

import android.arch.persistence.room.Room;
import android.content.Context;

import uz.N00003990.itripuz.room.database.AppDatabase;

/**
 * Created by Sherzodbek on 30.04.2018 in Lesson46ICT1.
 */
public class Database {
    private static AppDatabase database;

    public static void init(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context, AppDatabase.class, "data")
                    .allowMainThreadQueries()
                    .build();
        }
    }

    public static AppDatabase getDatabase() {
        return database;
    }
}
