package uz.N00003990.itripuz.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.room.dao.RecentlyViewedDao;


/**
 * Created by Sherzodbek on 30.04.2018 in Lesson46ICT1.
 */
@Database(entities = {RecentlyViewedData.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract RecentlyViewedDao getRecentlyViewedDao();
}
