package uz.N00003990.itripuz.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;


/**
 * Created by Sherzodbek on 30.04.2018 in Lesson46ICT1.
 */
@Dao
public interface RecentlyViewedDao {
    @Query("select * from last_data order by position desc")
    List<RecentlyViewedData> getViewedData();

    @Query("select * from last_data where favorite = 1  order by position desc")
    List<RecentlyViewedData> getFavoriteData();

    @Query("select * from last_data where name LIKE :name")
    RecentlyViewedData getData(String name);

    @Delete
    public void deleteViewedData(RecentlyViewedData data);

    @Insert()
    public void insertViewedData(RecentlyViewedData... data);

    @Update()
    void update(RecentlyViewedData... data);
}
