package uz.N00003990.itripuz.services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Timer;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.ScrollingActivity;

@SuppressLint("Registered")
public class CheckDatabaseService extends IntentService {
private static final String CHANNEL_ID = "500";
private static final int id= 1;
private Timer timer = new Timer();


    public CheckDatabaseService() {
        super("CheckDatabaseService");
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        startService(service);
        return super.startForegroundService(service);
    }



    private void createNotification(String title, String text, @DrawableRes int resId){
        Intent intent = new Intent(this, ScrollingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(resId)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
        notificationManager.notify(id, mBuilder.build());

    }

//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        super.onTaskRemoved(rootIntent);
//        startStopService();
//        Log.d("ppppp", "iwladi");
//    }
//
//    private void startStopService() {
//        startService(new Intent(this,CheckDatabaseService.class));
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MRX", "iwladi");
        sendRequest();
//        timer.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//            }
//        }, 0, 5000);//5 Seconds
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendRequest() {
        FirebaseDatabase.getInstance().getReference("cities")
                .addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
             Log.d("MRX","da="+dataSnapshot.getKey());
             createNotification("da","da",R.drawable.ic_hotel_black_24dp);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }
}
