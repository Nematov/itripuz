package uz.N00003990.itripuz.services;

import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Timer;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.ScrollingActivity;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class JobService extends android.app.job.JobService {
    private static final String CHANNEL_ID = "500";
    private static final int id = 1;
    private JobParameters params;
    private Timer timer = new Timer();
    private AttendanceCheckTask mAttendanceCheckTask;

    private class AttendanceCheckTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // some long running task...
            sendRequest();
            return null;
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            jobFinished(params, false);
        }
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        startForegroundService(service);
        return super.startForegroundService(service);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d("MRX", "ishladi");
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        this.params = params;
        mAttendanceCheckTask = new AttendanceCheckTask();
        mAttendanceCheckTask.execute();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        mAttendanceCheckTask.cancel(true);
        Log.d("MRX", "STOP");
        return false;
    }

    private void createNotification(String title, String text, @DrawableRes int resId) {
        Intent intent = new Intent(this, ScrollingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(resId)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
        notificationManager.notify(id, mBuilder.build());
    }


    private void sendRequest() {
        FirebaseDatabase.getInstance().getReference("cities")
                .addValueEventListener(new ValueEventListener() {


                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("MRX", "da=" + dataSnapshot.getKey());
                        createNotification("da", "da", R.drawable.ic_hotel_black_24dp);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });
    }
}
