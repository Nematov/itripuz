package uz.N00003990.itripuz.settings;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sherzodbek on 20.12.2017. (Lesson6ICT1)
 */

public class Settings {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static Settings settings;

    private Settings(Context context) {
        preferences = context.getSharedPreferences(getClass().getName(), Context.MODE_PRIVATE);
    }

    public static Settings getSettings() {
        return settings;
    }

    private void set(String key, String value) {
        editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void set(String key, int value) {
        editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private void set(String key, boolean value) {
        editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setKey(String key, String value) {
        set(key, value);
    }

    public void setKey(String key, boolean value) {
        set(key, value);
    }


    public void setKey(String key, int value) {
        set(key, value);
    }

    public int getInt(String key) {
        return preferences.getInt(key, 0);
    }

    public String getKey(String key) {
        return preferences.getString(key, "");
    }

    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

    public static void init(Context applicationContext) {
        if (settings == null) {
            settings = new Settings(applicationContext);
        }
    }
}
