package uz.N00003990.itripuz;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {
    private TextView subText;
    private TextView text1;
    private TextView text2;
    private ImageView imageView;
    private ViewGroup group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        subText = findViewById(R.id.sub_text);
        text1 = findViewById(R.id.splash_tex);
        text2 = findViewById(R.id.splash_text3);
        group = findViewById(R.id.splash_main);
        loadAnimate();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @SuppressLint({"NewApi", "SetTextI18n"})
    private void updateText() {
        subText.setText("Known better. Book better. Go better");
        subText.animate()
                .translationY(subText.getY() + 40)
                .setDuration(1500)
                .withEndAction(this::animateEnd)
                .start();

    }

    @SuppressLint("NewApi")
    private void animateEnd() {
        group.animate()
                .scaleXBy(7)
                .scaleYBy(7)
                .alpha(0)
                .setDuration(800)
                .withEndAction(this::startApp)
                .start();
    }

    @SuppressLint("NewApi")
    private void loadAnimate() {
        subText.animate()
                .translationY(-40)
                .setDuration(1500)
                .withEndAction(this::updateText)
                .start();
    }

    private void startApp() {
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }
}
