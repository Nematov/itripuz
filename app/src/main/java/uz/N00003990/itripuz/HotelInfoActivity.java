package uz.N00003990.itripuz;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import uz.N00003990.itripuz.custom_ui.CustomRatingBar;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.SelectData;
import uz.N00003990.itripuz.mvp.presenter_implement.HotelInfoActivityPresenterImpl;
import uz.N00003990.itripuz.mvp.views.HotelInfoActivityView;
import uz.N00003990.itripuz.room.database.Database;
import uz.N00003990.itripuz.settings.Settings;

public class HotelInfoActivity extends AppCompatActivity implements HotelInfoActivityView, CompoundButton.OnCheckedChangeListener {
    private HotelInfoActivityPresenterImpl presenter;

    private HotelData data;
    private RecentlyViewedData viewedData;
    private ImageView img;
    private ImageView hotel_favorite;
    private ImageView btnRatingDone;
    private TextView name;
    private TextView cost;
    private TextView review;
    private CustomRatingBar ratingBar;
    private Toolbar toolbar;
    private RadioGroup group;
    private RadioButton btn1;
    private RadioButton btn2;
    private RadioButton btn3;
    private RadioButton btn4;
    private RadioButton btn5;
    private List<SelectData> selectData;
    private SelectData selectHotel;
    private boolean isSelect = false;
    private int selectPosition;
    private boolean openSelect = false;
    private boolean isClickButtonRating = false;
    private boolean open = true;
    private boolean isLoadLastData = true;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_info);
        findViewById();
        loadView();
        loadData();

        loadRatingToView();
    }

    @SuppressLint("ResourceAsColor")
    private void loadView() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setSubtitleTextColor(R.color.colorWhite);
        setTitle("");

    }

    private void loadRatingToView() {
        btn1.setOnCheckedChangeListener(this);
        btn2.setOnCheckedChangeListener(this);
        btn3.setOnCheckedChangeListener(this);
        btn4.setOnCheckedChangeListener(this);
        btn5.setOnCheckedChangeListener(this);
        btn1.setText("1");
        btn2.setText("2");
        btn3.setText("3");
        btn4.setText("4");
        btn5.setText("5");
    }

    private boolean isHotelChecked() {
        for (int i = 0; i < selectData.size(); i++) {
            if (selectData.get(i).getName().equals(data.getName())) {
                selectPosition = i;
                selectHotel = selectData.get(selectPosition);
                return true;
            }
        }
        return false;
    }


    private void clickRatingBtns() {


        if (isHotelChecked()) {
//            btn1.setClickable(false);
//            btn2.setClickable(false);
//            btn3.setClickable(false);
//            btn4.setClickable(false);
//            btn5.setClickable(false);
            openSelect = true;
            isSelect = true;

            if (selectHotel.getRating() == Integer.parseInt(btn1.getText().toString())) {
                btn1.setChecked(true);
            }
            if (selectHotel.getRating() == Integer.parseInt(btn2.getText().toString())) {
                btn2.setChecked(true);
            }
            if (selectHotel.getRating() == Integer.parseInt(btn3.getText().toString())) {
                btn3.setChecked(true);
            }
            if (selectHotel.getRating() == Integer.parseInt(btn4.getText().toString())) {
                btn4.setChecked(true);
            }
            if (selectHotel.getRating() == Integer.parseInt(btn5.getText().toString())) {
                btn5.setChecked(true);
            }
            btnRatingDone.setImageResource(R.drawable.ic_done_all_black_24dp);
            isClickButtonRating = false;
        }

        btnRatingDone.setOnClickListener(v -> {
            if (isClickButtonRating) {

                if (isSelect) {
                    presenter.setHotelData(data);
                    presenter.setUserSelectHotel(selectData);
                    Log.d("MRX", "size-" + selectData.size());
                    isClickButtonRating = false;
                    Toast.makeText(this, "edited rating", Toast.LENGTH_SHORT).show();
                } else {
                    presenter.setUserSelectHotel(selectData);
                    data.setCount((data.getCount() + 1));
                    data.setSumRating(data.getSumRating() + selectData.get(selectData.size() - 1).getRating());
                    data.setRating((data.getSumRating()) / data.getCount());
                    presenter.setHotelData(data);
                    btnRatingDone.setImageResource(R.drawable.ic_done_all_black_24dp);
                    isClickButtonRating = false;
                    clickRatingBtns();
//               isSelect = true;
                }
            }
        });
    }

    private void loadData() {
        Bundle bundle = Objects.requireNonNull(getIntent().getExtras()).getBundle("DATA");
        assert bundle != null;
        data = (HotelData) bundle.getSerializable("DATA");
        presenter = new HotelInfoActivityPresenterImpl(this);
        presenter.loadDataRating();
        presenter.loadRecentlyViewData();

        hotel_favorite.setOnClickListener(v -> {

        viewedData = Database.getDatabase().getRecentlyViewedDao().getData(data.getName());
            if (viewedData.getFavorite() == 0){
                viewedData.setFavorite(1);
                hotel_favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                presenter.updateRecentlyViewedData(viewedData);
                Toast.makeText(this, "Added in favorite", Toast.LENGTH_SHORT).show();
            }
            else {
                viewedData.setFavorite(0);
                hotel_favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            presenter.updateRecentlyViewedData(viewedData);
                Toast.makeText(this, "Removed in favorite", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void findViewById() {
        toolbar = findViewById(R.id.toolbar);
        img = findViewById(R.id.app_bar_image);
        btnRatingDone = findViewById(R.id.btn_rtn);
        btnRatingDone.setClickable(true);
        name = findViewById(R.id.info_hotel_name);
        cost = findViewById(R.id.info_hotel_cost);
        review = findViewById(R.id.info_hotel_reviews);
        ratingBar = findViewById(R.id.rating);
        group = findViewById(R.id.group_rtn);
        hotel_favorite = findViewById(R.id.hotel_info_favorite);

        btn1 = findViewById(R.id.rtng1);
        btn2 = findViewById(R.id.rtng2);
        btn3 = findViewById(R.id.rtng3);
        btn4 = findViewById(R.id.rtng4);
        btn5 = findViewById(R.id.rtng5);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void loadDataToView(List<SelectData> selectData) {
        this.selectData = selectData;
        if (0 == Settings.getSettings().getInt(data.getName())) {
            data.setReviews(data.getReviews() + 1);
            presenter.setHotelData(data);
        }
        Settings.getSettings().setKey(data.getName(), data.getReviews());

        name.setText(data.getName());
//        cost.setText(data.getCost() + " UZS");
        review.setText(data.getReviews() + " Reviews");


        Picasso.get()
                .load(data.getImgUrl().get(0))
                .error(R.drawable.ic_error_black_24dp)
                .into(img);
        ratingBar.setRating(5, data.getRating());
        ratingBar.setColor(ContextCompat.getColor(this, R.color.colorRating));
        if (open) {
            open = false;
            clickRatingBtns();
        }

    }

    @Override
    public void setRecentlyViewData(List<RecentlyViewedData> recentlyViewData) {

        if (recentlyViewData.size() == 10) {
            RecentlyViewedData viewedData = recentlyViewData.get(recentlyViewData.size() - 1);
            presenter.deleteRecentlyViewData(viewedData);
        }
        boolean isHave = false;
        for (int i = 0; i < recentlyViewData.size(); i++) {
            RecentlyViewedData viewedData = recentlyViewData.get(i);
            if (data.getName().equals(viewedData.getName())) {
                presenter.deleteRecentlyViewData(viewedData);
                recentlyViewData.remove(viewedData);
                RecentlyViewedData viewedData1 = new RecentlyViewedData("hotel", "", data.getName(), data.getImgUrl().get(0), recentlyViewData.get(0).getPosition() + 1, data.getReviews(), data.getRating(), viewedData.getFavorite());
                presenter.setRecentlyViewData(viewedData1);
                EventBus.getDefault().post(viewedData1);
                isHave = true;
                    if (viewedData.getFavorite() == 1)
                        hotel_favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                    else
                        hotel_favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            }
        }

        if (!isHave) {
            RecentlyViewedData viewedData;
            if (recentlyViewData != null && recentlyViewData.size() > 0) {
                viewedData = new RecentlyViewedData("hotel","", data.getName(), data.getImgUrl().get(0), recentlyViewData.get(0).getPosition() + 1, data.getReviews(), data.getRating(), 0);
            } else
                viewedData = new RecentlyViewedData("hotel","", data.getName(), data.getImgUrl().get(0), 1, data.getReviews(), data.getRating(), 0);

            presenter.setRecentlyViewData(viewedData);
            EventBus.getDefault().post(viewedData);
            this.viewedData = viewedData;
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d("MRX", "change=" + buttonView.isChecked());
        if (isChecked && !openSelect) {

            if (selectData == null)
                selectData = new ArrayList<>();
            if (isSelect) {
                Log.d("MRX", "select=" + isSelect);
                isSelectEvent(Integer.parseInt(buttonView.getText().toString()));
            } else {
                Log.d("MRX", "select=" + isSelect);
                isClickButtonRating = true;
                selectData.add(new SelectData(data.getName(), Integer.parseInt(buttonView.getText().toString())));
            }
        }
        openSelect = false;
    }

    private void isSelectEvent(int newRating) {
        data.setSumRating(data.getSumRating() - selectData.get(selectPosition).getRating() + newRating);
        data.setRating(data.getSumRating() / data.getCount());
        selectData.get(selectPosition).setRating(newRating);
        Log.d("MRX", "size-" + selectData.size());
        isClickButtonRating = true;
    }
}
