package uz.N00003990.itripuz.models;

public class SendRecentlyData {
    private RecentlyViewedData data;
    private int position;

    public SendRecentlyData(RecentlyViewedData data, int position) {
        this.data = data;
        this.position = position;
    }

    public RecentlyViewedData getData() {
        return data;
    }

    public int getPosition() {
        return position;
    }
}
