package uz.N00003990.itripuz.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "last_data")
public class RecentlyViewedData implements Serializable {
    @PrimaryKey(autoGenerate = true)
   @ColumnInfo(name = "_id")
    private int id;
    private String type;
    private String city;
    private String name;
    @ColumnInfo(name = "img_url")
    private String imgUrl;
    private int position;
    private int reviews;
    private int rating;
    private int favorite;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Ignore
    public RecentlyViewedData() {
    }

    public RecentlyViewedData(String type, String city, String name, String imgUrl, int position, int reviews, int rating, int favorite) {
        this.type = type;
        this.city = city;
        this.name = name;
        this.imgUrl = imgUrl;
        this.position = position;
        this.reviews = reviews;
        this.rating = rating;
        this.favorite = favorite;
    }

    public void setReviews(int reviews) {
        this.reviews = reviews;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public int getFavorite() {
        return favorite;
    }

    public int getReviews() {
        return reviews;
    }

    public int getRating() {
        return rating;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {

        return position;
    }

    public String getType() {
        return type;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

}
