package uz.N00003990.itripuz.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

@IgnoreExtraProperties
public class UserRating {

    private List<SelectData> hotelsName;

    public List<SelectData> getHotelsName() {
        return hotelsName;
    }

    public void setHotels(List<SelectData> hotelsName) {
        this.hotelsName = hotelsName;
    }

    public void setHotelName(SelectData hotel) {
        hotelsName.add(hotel);
    }



    public UserRating() {
        hotelsName = new ArrayList<>();
    }

    }
