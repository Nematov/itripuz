package uz.N00003990.itripuz.models;

import java.io.Serializable;
import java.util.ArrayList;

public class RoomData implements Serializable {
    private Integer cost;
    private String name;
    private String category;
    private ArrayList<BusyData> busyData;

    public RoomData() {
    }

    public void setBusyData(ArrayList<BusyData> busyData) {
        this.busyData = busyData;
    }

    public ArrayList<BusyData> getBusyData() {
        return busyData;
    }

    public String getCategory() {
        return category;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public RoomData(Integer cost, String name, String category, ArrayList<BusyData> busyData) {
        this.cost = cost;
        this.name = name;
        this.category = category;
        this.busyData = busyData;
    }

    public Integer getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
