package uz.N00003990.itripuz.models;

public class VacationData {
    private String name;
    private String imgUrl;

    public VacationData(String name, String imgUrl, String address, Integer review) {
        this.name = name;
        this.imgUrl = imgUrl;
        this.address = address;
        this.review = review;
    }

    private String address;
    private Integer review;

    public VacationData() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setReview(Integer review) {
        this.review = review;
    }

    public String getName() {
        return name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public Integer getReview() {
        return review;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
