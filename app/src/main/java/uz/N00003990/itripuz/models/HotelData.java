package uz.N00003990.itripuz.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

@IgnoreExtraProperties
public class HotelData implements Serializable {
    private String uId;
    private String managerNumber;
    private String name;
    private String address;
    private Integer reviews;
    private ArrayList<String> imgUrl;
    private Map<String, RoomData> rooms;
    private int rating;
    private int count;
    private int sumRating;

    public HotelData() {
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public int getSumRating() {
        return sumRating;
    }

    public void setSumRating(int sumRating) {
        this.sumRating = sumRating;
    }

    public HotelData(String uId, String name, String address, Integer reviews, ArrayList<String> imgUrl, Map<String, RoomData> rooms, int rating, int count, int sumRating) {
        this.uId = uId;
        this.name = name;
        this.address = address;
        this.reviews = reviews;
        this.imgUrl = imgUrl;
        this.rooms = rooms;
        this.rating = rating;
        this.count = count;
        this.sumRating = sumRating;
    }

    public String getAddress() {
        return address;
    }

    public Map<String, RoomData> getRooms() {
        return rooms;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRooms(Map<String, RoomData> rooms) {
        this.rooms = rooms;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setReviews(Integer reviews) {
        this.reviews = reviews;
    }

    public void setImgUrl(ArrayList<String> imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {

        return count;
    }

    public String getName() {
        return name;
    }

    public Integer getReviews() {
        return reviews;
    }

    public ArrayList<String> getImgUrl() {
        return imgUrl;
    }

    public Integer getRating() {
        return rating;
    }

}
