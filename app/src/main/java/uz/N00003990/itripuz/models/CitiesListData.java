package uz.N00003990.itripuz.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties
public class CitiesListData {
    private List<CitiesData> cities;

    public CitiesListData() {
    }

    public List<CitiesData> getCities() {
        return cities;
    }
}
