package uz.N00003990.itripuz.models;

import java.io.Serializable;

public class BusyData implements Serializable {
    private String name;
    private String date;
    private String phoneNumber;
    private String passport;

    public String getPassport() {
        return passport;
    }

    public BusyData(String name, String date, String phoneNumber, String passport) {
        this.name = name;
        this.date = date;
        this.phoneNumber = phoneNumber;
        this.passport = passport;
    }

    public BusyData() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
