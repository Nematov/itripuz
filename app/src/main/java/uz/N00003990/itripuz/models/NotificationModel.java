package uz.N00003990.itripuz.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationModel {
    private String userId;
    private ArrayList<BusyData> busyData;
    private String roomName;
    private String hotelName;
    private String managerId;
    private String managerAnswered;
    private String accepted;
    private String key;
    private String whoFor;

    public void setWhoFor(String whoFor) {
        this.whoFor = whoFor;
    }

    public String getWhoFor() {
        return whoFor;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    private boolean read;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setBusyData(ArrayList<BusyData> busyData) {
        this.busyData = busyData;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public void setManagerAnswered(String managerAnswered) {
        this.managerAnswered = managerAnswered;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getUserId() {
        return userId;
    }

    public ArrayList<BusyData> getBusyData() {
        return busyData;
    }

    public String getRoomName() {
        return roomName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getManagerId() {
        return managerId;
    }

    public String getManagerAnswered() {
        return managerAnswered;
    }

    public String getAccepted() {
        return accepted;
    }

    public boolean isRead() {
        return read;
    }

    public NotificationModel() {
    }

    public NotificationModel(String userId, ArrayList<BusyData> busyData, String roomName, String hotelName, String managerId, String managerAnswered, String accepted, boolean read, String whoFor) {
        this.userId = userId;
        this.busyData = busyData;
        this.roomName = roomName;
        this.hotelName = hotelName;
        this.managerId = managerId;
        this.managerAnswered = managerAnswered;
        this.accepted = accepted;
        this.read = read;
        this.whoFor = whoFor;
    }
}
