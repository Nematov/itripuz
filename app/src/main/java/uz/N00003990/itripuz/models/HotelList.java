package uz.N00003990.itripuz.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;
@IgnoreExtraProperties
public class HotelList {
    private List<HotelData> hotels;

    public HotelList() {
    }

    public void setHotels(List<HotelData> hotels) {
        this.hotels = hotels;
    }

    public List<HotelData> getHotels() {
        return hotels;
    }
}
