package uz.N00003990.itripuz.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties
public class CitiesData {
    private Integer cityId;
    private String name;
    private String imgUrl;
    private List<HotelList> hotelList;

    public CitiesData() {
    }

    public CitiesData(Integer cityId, String name, String imgUrl) {
        this.cityId = cityId;
        this.name = name;
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public CitiesData(Integer cityId, String name) {
        this.cityId = cityId;
        this.name = name;
    }

    public List<HotelList> getHotelList() {
        return hotelList;
    }

    public Integer getCityId() {
        return cityId;
    }

    public String getName() {
        return name;
    }
}
