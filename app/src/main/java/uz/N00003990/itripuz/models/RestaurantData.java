package uz.N00003990.itripuz.models;

import java.io.Serializable;

public class RestaurantData implements Serializable {
    private int restaurantId;
    private String name;
    private String imgUrl;
    private int rating;
    private int Review;
    private float locLatitude;
    private float locLongitude;
    private int count;
    private int sumRating;
    private String city;

    public RestaurantData(int restaurantId, String name, String imgUrl, int rating, int review) {
        this.restaurantId = restaurantId;
        this.name = name;
        this.imgUrl = imgUrl;
        this.rating = rating;
        Review = review;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setReview(int review) {
        Review = review;
    }

    public void setLocLatitude(float locLatitude) {
        this.locLatitude = locLatitude;
    }

    public void setLocLongitude(float locLongitude) {
        this.locLongitude = locLongitude;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setSumRating(int sumRating) {
        this.sumRating = sumRating;
    }

    public float getLocLatitude() {
        return locLatitude;
    }

    public float getLocLongitude() {
        return locLongitude;
    }

    public int getCount() {
        return count;
    }

    public int getSumRating() {
        return sumRating;
    }

    public RestaurantData() {
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public String getName() {
        return name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public int getReview() {
        return Review;
    }

    public int getRating() {
        return rating;
    }
}
