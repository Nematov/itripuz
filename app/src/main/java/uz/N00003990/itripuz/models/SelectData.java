package uz.N00003990.itripuz.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class SelectData {
    private String name;
    private int rating;

    public SelectData(String name, int rating) {
        this.name = name;
        this.rating = rating;
    }

    public SelectData() {
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
