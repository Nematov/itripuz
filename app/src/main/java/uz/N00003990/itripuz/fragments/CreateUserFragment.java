package uz.N00003990.itripuz.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import uz.N00003990.itripuz.R;

public class CreateUserFragment extends Fragment {
    private AutoCompleteTextView eMail;
    private EditText mPasswordView;

    private FirebaseAuth mAuth;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_user, container, false);
        eMail = view.findViewById(R.id.login);
        context = container.getContext();
        mPasswordView = view.findViewById(R.id.password);
        Button createButton = view.findViewById(R.id.create_button);
        mAuth = FirebaseAuth.getInstance();

        createButton.setOnClickListener(this::createClick);
        return view;
    }

    private void createClick(View view) {
        if (eMail.length() > 0 && mPasswordView.length() > 6){

        mAuth.createUserWithEmailAndPassword(eMail.getText().toString(), mPasswordView.getText().toString())
                .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("MRX", "createUserWithEmail:success");
                        getActivity().getSupportFragmentManager().popBackStack();
                        Toast.makeText(context, "Created user", Toast.LENGTH_SHORT).show();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("MRX", "createUserWithEmail:failure", task.getException());
                        Toast.makeText(context, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }

                    // ...
                });
        }
        else {
            Toast.makeText(context, "Sorry,email or password don't write", Toast.LENGTH_SHORT).show();
        }
    }
}
