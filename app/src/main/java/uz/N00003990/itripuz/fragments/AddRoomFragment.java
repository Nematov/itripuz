package uz.N00003990.itripuz.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RoomData;
import uz.N00003990.itripuz.mvp.presenter_implement.AddRoomPresenterImpl;
import uz.N00003990.itripuz.mvp.views.AddRoomFragmentView;

public class AddRoomFragment extends BaseFragment implements AddRoomFragmentView {
    private AddRoomPresenterImpl presenter;
    private String category;
    private EditText name;
    private EditText cost;
    private EditText count;
    private Button button;
    private HotelData hotelData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_room, container, false);
        presenter = new AddRoomPresenterImpl(this);
        category = getArguments().getString("category");
        name = view.findViewById(R.id.name);
        cost = view.findViewById(R.id.cost);
        count = view.findViewById(R.id.count);
        button = view.findViewById(R.id.save_room);
        hotelData = (HotelData) getArguments().getSerializable("data");
        button.setOnClickListener(this::clickSaveButton);
        getActivity().setTitle("Add Rooms");
        return view;
    }


    private void clickSaveButton(View view) {
        boolean isEmptyName = TextUtils.isEmpty(name.getText());
        boolean isEmptyCost = TextUtils.isEmpty(cost.getText());
        boolean isEmptyCount = TextUtils.isEmpty(count.getText());
        if (isEmptyName) name.setError("Write name room");
        if (isEmptyCost) cost.setError("Write cost room");
        if (isEmptyCount) count.setError("Write count room");
        if (!isEmptyName && !isEmptyCost && !isEmptyCount) {
            int a = Integer.parseInt(cost.getText().toString());
            int c = Integer.parseInt(count.getText().toString());
            if (hotelData.getRooms() == null)
                hotelData.setRooms(new HashMap<>());
            for (int i = 0; i < c; i++) {
                RoomData roomData = new RoomData(a, name.getText().toString() + category + i, category, new ArrayList<>());
                hotelData.getRooms().put(roomData.getName(), roomData);
            }

            presenter.saveRoom(hotelData.getName(), hotelData.getRooms());
        }
    }

    public static Fragment getInstance(String category, HotelData hotelData) {
        Fragment fragment = new AddRoomFragment();
        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        bundle.putSerializable("data", hotelData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected boolean isToolbarShow() {
        return false;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    public void onSuccessRoom() {
        getActivity().getSupportFragmentManager().popBackStack();
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onErrorRoom() {

    }
}
