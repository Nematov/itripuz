package uz.N00003990.itripuz.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;
import java.util.Objects;

import uz.N00003990.itripuz.HotelInfoActivity;
import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.HotelAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.holders.HotelHolder;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.mvp.presenter_implement.HotelFragmentPresenterImpl;
import uz.N00003990.itripuz.mvp.views.HotelFragmentView;
import uz.N00003990.itripuz.settings.Settings;

public class HotelFragment extends BaseFragment implements HotelHolder.OnClickItemHotelListener, HotelFragmentView {
    private HotelFragmentPresenterImpl presenter;

    private RecyclerView list;
    private HotelAdapter hotelAdapter;
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @SuppressLint({"CommitTransaction", "NewApi", "ResourceAsColor"})
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel, container, false);
        findViewById(view);
        presenter = new HotelFragmentPresenterImpl(HotelFragment.this);
        context = container.getContext();
        presenter.updateHotelData();
        Settings.getSettings().setKey("admin", false);
        return view;
    }

    @SuppressLint("ResourceAsColor")
    private void findViewById(View view) {
        list = view.findViewById(R.id.list_hotel);
        list.setLayoutManager(new LinearLayoutManager(context));
    }

    private void openMap(View view) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(R.id.fragment_activity_layout, new MapFragment(), "map");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void refreshData() {
        hotelAdapter.notifyDataSetChanged();
    }


    @SuppressLint("CommitTransaction")
    @Override
    public void onClickItem(int position, HotelData data) {
        Intent intent = new Intent(getContext(), HotelInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA", data);
        intent.putExtra("DATA", bundle);
        startActivity(intent);
    }

    @Override
    public void onClickBusy(int position, HotelData data) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_activity_layout, HotelRoomFragment.getInstance(data));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onClickViewItem(int position, HotelData data) {

    }

    @Override
    public void onClickDelete(int position, HotelData data) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MRX", "menu" + item.getItemId());
        if (item.getItemId() == android.R.id.home) {
            Objects.requireNonNull(getActivity()).finish();
            return true;
        }
        if (item.getItemId() == R.id.search_menu) {
            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.fragment_activity_layout, new SearchFragment());
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    protected boolean isToolbarShow() {
        return true;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return true;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return true;
    }

    @Override
    public void getViewListHotelData(List<HotelData> data) {

        if (hotelAdapter == null) {
            hotelAdapter = new HotelAdapter(data);
            hotelAdapter.setOnClickItemHotelListener(HotelFragment.this);
            list.setAdapter(hotelAdapter);
        } else {
            hotelAdapter.notifyDataSetChanged();
        }
    }


}
