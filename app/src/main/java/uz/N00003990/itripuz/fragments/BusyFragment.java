package uz.N00003990.itripuz.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.BusyAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.dialogs.BusyDialog;
import uz.N00003990.itripuz.dialogs.CalendarDialog;
import uz.N00003990.itripuz.dialogs.Dialog;
import uz.N00003990.itripuz.dialogs.LoadingDialog;
import uz.N00003990.itripuz.models.BusyData;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.NotificationModel;
import uz.N00003990.itripuz.models.RoomData;
import uz.N00003990.itripuz.mvp.presenter_implement.BusyPresenterImpl;
import uz.N00003990.itripuz.mvp.views.BusyFragmentView;
import uz.N00003990.itripuz.retrofit.models.MessageData;
import uz.N00003990.itripuz.settings.Settings;

public class BusyFragment extends BaseFragment implements BusyFragmentView {
    private BusyPresenterImpl presenter;

    private HotelData hotelName;
    private RoomData roomData;
    private RecyclerView recyclerView;
    private TextView textView;
    private BusyAdapter busyAdapter;
    private CalendarDialog calendarDialog;
    private Button button;
    private BusyDialog busyDialog;
    private String selectedDate;
    private ArrayList<BusyData> busyList;
    private LoadingDialog loadingDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_busy, container, false);
        recyclerView = view.findViewById(R.id.list);
        textView = view.findViewById(R.id.select_day);
        button = view.findViewById(R.id.busy);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getActivity().setTitle("Room's busy");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hotelName = (HotelData) getArguments().getSerializable("key");
        roomData = (RoomData) getArguments().getSerializable("data");
        calendarDialog = new CalendarDialog(getContext());
        presenter = new BusyPresenterImpl(this);
        textView.setText(calendarDialog.getDate());
        selectedDate = calendarDialog.getDate();

        presenter.getList(hotelName.getName(), roomData.getName(), calendarDialog.getDate());
        button.setOnClickListener(this::toBusy);
        loadingDialog = new LoadingDialog(getContext());
    }

    private void toBusy(View view) {
        busyDialog = new BusyDialog(getContext());
        busyDialog.setOnClickListener(new BusyDialog.OnClickListener() {
            @Override
            public void Apply() {
                if (busyDialog.isEmptyFields()) {
                    for (int i = 0; i < busyDialog.getDay(); i++) {
                        long l = calendarDialog.converTimeSeconds(selectedDate);
                        l += i * TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);
                        String d = calendarDialog.convertDateString(l);
                        BusyData busyData = new BusyData(busyDialog.getName(), d, busyDialog.getPhone(), busyDialog.getPassport());
                        if (busyList != null) {
                            busyList.add(busyData);
                        } else {
                            busyList = new ArrayList<>();
                            busyList.add(busyData);
                        }
                    }


                    if (Settings.getSettings().getBoolean("admin")) {
                        presenter.setBusy(busyList, hotelName.getName(), roomData.getName());
                        busyDialog.hide();
                    } else {
                        NotificationModel notificationModel = new NotificationModel(FirebaseAuth.getInstance().getCurrentUser().getUid()
                                , busyList
                                , roomData.getName()
                                , hotelName.getName()
                                , hotelName.getuId()
                                , "No answer"
                                , "no"
                                , false
                                , FirebaseAuth.getInstance().getCurrentUser().getUid()
                        );
                        MessageData messageData = new MessageData("/topics/" + hotelName.getuId()
                                , notificationModel);
                        presenter.sendNotification(messageData);
                        loadingDialog.show();
                    }
                }
            }

            @Override
            public void Cancel() {
                busyDialog.hide();
            }

            @Override
            public void changeDayTextListener() {
                busyDialog.setTotal(String.valueOf(busyDialog.getDay() * roomData.getCost()) + "$");
            }
        });
        busyDialog.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_calendar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.calendar) {
            calendarDialog.show();
            calendarDialog.setOnClickListener(new Dialog.OnClickListener() {
                @Override
                public void Apply() {
                    textView.setText(calendarDialog.getSelectDate());
                    presenter.getList(hotelName.getName(), roomData.getName(), calendarDialog.getSelectDate());
                    calendarDialog.hide();
                    selectedDate = calendarDialog.getSelectDate();
                }

                @Override
                public void Cancel() {
                    calendarDialog.hide();
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    public static Fragment getInstance(HotelData hotelData, RoomData roomData) {
        Fragment fragment = new BusyFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", roomData);
        bundle.putSerializable("key", hotelData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected boolean isToolbarShow() {
        return !Settings.getSettings().getBoolean("admin");
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    public void setBusyList(ArrayList<BusyData> busyList) {
        this.busyList = busyList;
        busyAdapter = new BusyAdapter(busyList);
        recyclerView.setAdapter(busyAdapter);
    }

    @Override
    public void onSendNotification() {
        loadingDialog.hide();
        busyDialog.hide();
    }

    @Override
    public void onError() {
        loadingDialog.hide();
        Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();
    }
}
