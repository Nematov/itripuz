package uz.N00003990.itripuz.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.AddHotelImageAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.dialogs.LoadingDialog;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RoomData;
import uz.N00003990.itripuz.mvp.presenter_implement.AddHotelPresenterImpl;
import uz.N00003990.itripuz.mvp.views.AddHotelFragmentView;

public class AddHotelFragment extends BaseFragment implements AddHotelFragmentView {
    private AddHotelPresenterImpl presenter;
    private int GALLERY = 1;
    private File file;
    private Uri uri;
    private ArrayList<String> imageUrl;
    private EditText name;
    private EditText address;
    private TextView selectImage;
    private Button button;
    private String uId;
    private RecyclerView listImage;
    private AddHotelImageAdapter addHotelImageAdapter;
    private ImageView imageView;
    private LoadingDialog loadingDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_hotel, container, false);
        name = view.findViewById(R.id.name);
        listImage = view.findViewById(R.id.list_image);
        imageView = view.findViewById(R.id.image);
//        selectImage = view.findViewById(R.id.select_image);
        address = view.findViewById(R.id.address);
        button = view.findViewById(R.id.save);
        imageView.setOnClickListener(this::choosePhotoFromGallary);
        getActivity().setTitle("Add Hotels");
        imageUrl = new ArrayList<>();
        addHotelImageAdapter = new AddHotelImageAdapter(imageUrl);
        listImage.setLayoutManager(new GridLayoutManager(getContext(), 1, LinearLayoutManager.HORIZONTAL, true));
        listImage.setAdapter(addHotelImageAdapter);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().setTitle("My Hotels");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new AddHotelPresenterImpl(this);
        uId = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
        presenter.getMyHotelList(uId);
        loadingDialog = new LoadingDialog(getContext());
        button.setOnClickListener(this::onClickSave);
    }

    private void onClickSave(View view) {
        Editable a = address.getText();
        Editable n = name.getText();
        boolean isEmptyAddres = TextUtils.isEmpty(a);
        boolean isEmptyName = TextUtils.isEmpty(n);
        if (isEmptyAddres) {
            address.setError("write address");
        }
        if (isEmptyName) {
            name.setError("write hotel name");
        }
        if (!isEmptyAddres && !isEmptyName) {
            HotelData hotelData = new HotelData(uId, n.toString(), a.toString(), 0, imageUrl,
                    new HashMap<>(), 0, 0, 0);
            presenter.saveDataToDatabase(hotelData);
            loadingDialog.show();
        }
    }

    void choosePhotoFromGallary(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY && data != null) {
            uri = data.getData();
            String path = getRealPathFromURI(uri);
            file = new File(path);
            presenter.uploadImage(uri);

            loadingDialog.show();
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = new String[]{MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri,
                proj, // WHERE clause selection arguments (none)
                null, null, null);// Which columns to return
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    @Override
    protected boolean isToolbarShow() {
        return false;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    public void onSuccessUploadImage(String url) {
        imageUrl.add(url);
        addHotelImageAdapter.notifyDataSetChanged();
        loadingDialog.hide();
//        selectImage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_file_download_black_24dp,
//                0, R.drawable.ic_check_circle_black_24dp, 0);
    }

    @Override
    public void onErrorUploadImage() {

    }

    @Override
    public void onSuccessSaveDataToDatabase() {
        loadingDialog.hide();
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onErrorSaveDataToDatabase() {

    }

    @Override
    public void setDataToView(ArrayList<HotelData> data) {

    }

    @Override
    public void noResult() {

    }
}
