package uz.N00003990.itripuz.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.HotelInfoActivity;
import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.HotelAdapter;
import uz.N00003990.itripuz.holders.HotelHolder;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.mvp.presenter_implement.SearchFragmentPresenterImpl;
import uz.N00003990.itripuz.mvp.views.SearchFragmentView;

public class SearchFragment extends Fragment implements HotelHolder.OnClickItemHotelListener, SearchFragmentView {

    private SearchFragmentPresenterImpl presenter;

    private SearchView search;
    private RecyclerView list;
    private List<HotelData> data;
    private HotelAdapter adapter;
    private TextView textView;
    private AVLoadingIndicatorView loadingIndicatorView;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        findViewById(view);
        assert container != null;
        context = container.getContext();
        eventLoadElement();


        return view;
    }

    private void findViewById(View view) {
        search = view.findViewById(R.id.search);
        list = view.findViewById(R.id.list);
        textView = view.findViewById(R.id.text);
        loadingIndicatorView = view.findViewById(R.id.loaderIndicator);
    }

    private void eventLoadElement() {
        presenter = new SearchFragmentPresenterImpl(SearchFragment.this);
        data = new ArrayList<>();
        list.setLayoutManager(new LinearLayoutManager(context));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 0) {

                    textView.setVisibility(View.GONE);
                    loadingIndicatorView.setVisibility(View.VISIBLE);
                    presenter.loadData(newText);

                } else {
                    if (data.size() > 0)
                        data.clear();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                }
                return true;
            }
        });
    }


    @Override
    public void noResult() {

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        new CountDownTimer(2000, 100) {

            @Override
            public void onTick(long time) {
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFinish() {
                textView.setVisibility(View.VISIBLE);
                textView.setText("No Result");
                loadingIndicatorView.setVisibility(View.INVISIBLE);

            }
        }.start();
    }

    @Override
    public void setDataToView(List<HotelData> data) {
        if (adapter == null) {
            adapter = new HotelAdapter(data);
            adapter.setOnClickItemHotelListener(SearchFragment.this);
            list.setAdapter(adapter);
            loadingIndicatorView.setVisibility(View.INVISIBLE);
        } else {
            adapter.notifyDataSetChanged();
            loadingIndicatorView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClickItem(int p,HotelData data) {
        Intent intent = new Intent(context, HotelInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA", data);
        intent.putExtra("DATA", bundle);
        startActivity(intent);
    }

    @Override
    public void onClickBusy(int position, HotelData data) {

    }

    @Override
    public void onClickViewItem(int position,HotelData hotelData) {

    }

    @Override
    public void onClickDelete(int position, HotelData data) {

    }
}
