package uz.N00003990.itripuz.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.VacationAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.holders.VacationHolder;
import uz.N00003990.itripuz.models.VacationData;
import uz.N00003990.itripuz.mvp.presenter_implement.VacationPresenterImpl;
import uz.N00003990.itripuz.mvp.views.VacationView;

public class VacationFragment extends BaseFragment implements VacationView {
    private VacationPresenterImpl presenter;
    private RecyclerView recyclerView;
    private VacationAdapter vacationAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vacation, container, false);
        recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        presenter = new VacationPresenterImpl(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        presenter.loadData();
    }

    @Override
    protected boolean isToolbarShow() {
        return true;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    public void setData(ArrayList<VacationData> data) {
        vacationAdapter = new VacationAdapter(data);
        vacationAdapter.setOnClickItemListener(data1 -> {

        });
        recyclerView.setAdapter(vacationAdapter);
    }
}
