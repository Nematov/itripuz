package uz.N00003990.itripuz.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.InfoActivity;
import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.RestaurantAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.holders.RestaurantHolder;
import uz.N00003990.itripuz.models.RestaurantData;
import uz.N00003990.itripuz.mvp.presenter.RestaurentFragmentPresenter;
import uz.N00003990.itripuz.mvp.presenter_implement.RestaurantFragmentPresenterImpl;
import uz.N00003990.itripuz.mvp.views.RestaurantFragmentView;

public class RestaurantFragment extends BaseFragment implements RestaurantFragmentView, RestaurantHolder.OnClickItemListener {
    private RestaurentFragmentPresenter presenter;


    private RestaurantAdapter adapter;
    private Context context;
    private RecyclerView list;
    private List<RestaurantData> data;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);
        context = container.getContext();
        loadView(view);
        presenter = new RestaurantFragmentPresenterImpl(this);
        presenter.loadData();


        return view;
    }

    private void loadView(View view) {
        list = view.findViewById(R.id.list_restaurant);
        list.setLayoutManager(new LinearLayoutManager(context));
        data = new ArrayList<>();
    }

    @Override
    protected boolean isToolbarShow() {
        return true;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return true;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return true;
    }

    @Override
    public void setDataToView(List<RestaurantData> data) {
        this.data = data;
        if (adapter == null) {
            adapter = new RestaurantAdapter(data);
            adapter.setOnClickItemListener(RestaurantFragment.this);
            list.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClickItem(RestaurantData data) {
        Intent intent = new Intent(getContext(), InfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA",data);
        intent.putExtra("DATA",bundle);
        startActivity(intent);
//        FragmentManager manager = getActivity().getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(R.id.fragment_activity_layout,RestaurantInfoFragment.getInstance(data));
//        transaction.addToBackStack(null);
//        transaction.commit();
    }
}
