package uz.N00003990.itripuz.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.RoomAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.holders.RoomHolder;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RoomData;
import uz.N00003990.itripuz.settings.Settings;

public class HotelRoomFragment extends BaseFragment implements RoomHolder.OnClickRoomItemListener {
    private RecyclerView recyclerView;
    private Spinner spinner;
    private FloatingActionButton floatingActionButton;
    private HotelData hotelData;
    private RoomAdapter roomAdapter;
    private Dialog dialog;
    private ArrayList<String> spinnerList;
    private ArrayList<RoomData> singleRoom;
    private ArrayList<RoomData> doubleRoom;
    private ArrayList<RoomData> kingRoom;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel_room, container, false);
        recyclerView = view.findViewById(R.id.list);
        spinner = view.findViewById(R.id.spinner);
        floatingActionButton = view.findViewById(R.id.add_button);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        hotelData = (HotelData) getArguments().getSerializable("data");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        floatingActionButton.setOnClickListener(this::showDialogCategory);
        if (!Settings.getSettings().getBoolean("admin"))
            floatingActionButton.setVisibility(View.GONE);
        if (hotelData.getRooms() != null) {
            sortRoomsByCategory();
            initSpinner();

        }
        getActivity().setTitle("Rooms");
    }

    private void sortRoomsByCategory() {
        singleRoom = new ArrayList<>();
        doubleRoom = new ArrayList<>();
        kingRoom = new ArrayList<>();
        String[] keys = hotelData.getRooms().keySet().toArray(new String[hotelData.getRooms().size()]);
        for (int i = 0; i < hotelData.getRooms().size(); i++) {
            switch (hotelData.getRooms().get(keys[i]).getCategory()) {
                case "Single Room":
                    singleRoom.add(hotelData.getRooms().get(keys[i]));
                    break;
                case "Double Room":
                    doubleRoom.add(hotelData.getRooms().get(keys[i]));
                    break;
                case "King Room":
                    kingRoom.add(hotelData.getRooms().get(keys[i]));
                    break;
            }
        }
    }

    private void initSpinner() {
        spinnerList = new ArrayList<>();
        spinnerList.add("Single Room");
        spinnerList.add("Double Room");
        spinnerList.add("King Room");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, spinnerList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        roomAdapter = new RoomAdapter(singleRoom);
                        break;
                    case 1:
                        roomAdapter = new RoomAdapter(doubleRoom);
                        break;
                    case 2:
                        roomAdapter = new RoomAdapter(kingRoom);
                        break;
                }
                roomAdapter.setOnClickRoomItemListener(HotelRoomFragment.this);
                recyclerView.setAdapter(roomAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_notification, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.notify) {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            if (Settings.getSettings().getBoolean("admin"))
                transaction.add(R.id.container, new NotifFragment());
            else
                transaction.add(R.id.fragment_activity_layout, new NotifFragment());
            transaction.addToBackStack(null);
            transaction.commit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().setTitle("My Hotels");
    }

    private void showDialogCategory(View view) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_category_room);
        dialog.findViewById(R.id.single_room).setOnClickListener(this::clickDialogItem);
        dialog.findViewById(R.id.double_room).setOnClickListener(this::clickDialogItem);
        dialog.findViewById(R.id.king_room).setOnClickListener(this::clickDialogItem);
        dialog.show();
    }

    public static Fragment getInstance(HotelData hotelData) {
        Fragment fragment = new HotelRoomFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", hotelData);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void clickDialogItem(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        switch (view.getId()) {
            case R.id.single_room:
                transaction.replace(R.id.container, AddRoomFragment.getInstance("Single Room", hotelData));
                break;
            case R.id.double_room:
                transaction.replace(R.id.container, AddRoomFragment.getInstance("Double Room", hotelData));
                break;
            case R.id.king_room:
                transaction.replace(R.id.container, AddRoomFragment.getInstance("King Room", hotelData));
                break;
        }
        transaction.addToBackStack(null);
        transaction.commit();
        dialog.hide();
    }

    @Override
    protected boolean isToolbarShow() {
        return !Settings.getSettings().getBoolean("admin");
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    public void onClickIcon(RoomData roomData) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (Settings.getSettings().getBoolean("admin"))
            transaction.add(R.id.container, BusyFragment.getInstance(hotelData, roomData));
        else
            transaction.add(R.id.fragment_activity_layout, BusyFragment.getInstance(hotelData, roomData));
        transaction.addToBackStack(null);
        transaction.commit();
    }
}