package uz.N00003990.itripuz.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.custom_ui.CustomRatingBar;
import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.RestaurantData;
import uz.N00003990.itripuz.models.SelectData;
import uz.N00003990.itripuz.mvp.presenter_implement.RestaurantInfoFragmentPresenterImpl;
import uz.N00003990.itripuz.mvp.views.RestaurantInfoFragmentView;
import uz.N00003990.itripuz.settings.Settings;

public class RestaurantInfoFragment extends BaseFragment implements RestaurantInfoFragmentView, CompoundButton.OnCheckedChangeListener {
    private RestaurantInfoFragmentPresenterImpl presenter;

    private ImageView img;
    private ImageView btnRatingDone;
    private TextView name;
    private TextView review;
    private CustomRatingBar ratingBar;
    private RestaurantData data;
    private List<SelectData> selectData;
    private RadioGroup group;
    private RadioButton btn1;
    private RadioButton btn2;
    private RadioButton btn3;
    private RadioButton btn4;
    private RadioButton btn5;
    private SelectData selectRestaurant;
    private boolean isSelect = false;
    private int selectPosition;
    private boolean openSelect = false;
    private boolean isClickButtonRating = false;
    private boolean open = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant_info, container, false);
        findViewById(view);
        assert getArguments() != null;
        data = (RestaurantData) getArguments().getSerializable("DATA");
        setDataToView();
        loadRatingToView();
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void setDataToView() {
        presenter = new RestaurantInfoFragmentPresenterImpl(this);

        if (0 == Settings.getSettings().getInt(data.getName())) {
            data.setReview(data.getReview() + 1);
            presenter.setRestaurantData(data);
        }
        Settings.getSettings().setKey(data.getName(), data.getReview());
        name.setText(data.getName());
        review.setText(data.getReview() + " Review");
        ratingBar.setRating(5, data.getRating());
        ratingBar.setColor(ContextCompat.getColor(getContext(), R.color.colorRating));
        Picasso.get()
                .load(data.getImgUrl())
                .into(img);
        presenter.loadData();
        presenter.loadRecentlyData();
    }

    private void findViewById(View view) {
        img = view.findViewById(R.id.app_bar_image);
        name = view.findViewById(R.id.info_restaurant_name);
        review = view.findViewById(R.id.info_restaurant_reviews);
        ratingBar = view.findViewById(R.id.rating);
        ratingBar = view.findViewById(R.id.rating);
        group = view.findViewById(R.id.group_rtn);
        btnRatingDone = view.findViewById(R.id.btn_rtn);
        btnRatingDone.setClickable(true);
        btn1 = view.findViewById(R.id.rtng1);
        btn2 = view.findViewById(R.id.rtng2);
        btn3 = view.findViewById(R.id.rtng3);
        btn4 = view.findViewById(R.id.rtng4);
        btn5 = view.findViewById(R.id.rtng5);
    }

    private void loadRatingToView() {
        btn1.setOnCheckedChangeListener(this);
        btn2.setOnCheckedChangeListener(this);
        btn3.setOnCheckedChangeListener(this);
        btn4.setOnCheckedChangeListener(this);
        btn5.setOnCheckedChangeListener(this);
        btn1.setText("1");
        btn2.setText("2");
        btn3.setText("3");
        btn4.setText("4");
        btn5.setText("5");
    }

    private boolean isHotelChecked() {
        for (int i = 0; i < selectData.size(); i++) {
            if (selectData.get(i).getName().equals(data.getName())) {
                selectPosition = i;
                selectRestaurant = selectData.get(selectPosition);
                return true;
            }
        }
        return false;
    }

    private void clickRatingBtns() {
        if (isHotelChecked()) {
            openSelect = true;
            isSelect = true;

            if (selectRestaurant.getRating() == Integer.parseInt(btn1.getText().toString())) {
                btn1.setChecked(true);
            }
            if (selectRestaurant.getRating() == Integer.parseInt(btn2.getText().toString())) {
                btn2.setChecked(true);
            }
            if (selectRestaurant.getRating() == Integer.parseInt(btn3.getText().toString())) {
                btn3.setChecked(true);
            }
            if (selectRestaurant.getRating() == Integer.parseInt(btn4.getText().toString())) {
                btn4.setChecked(true);
            }
            if (selectRestaurant.getRating() == Integer.parseInt(btn5.getText().toString())) {
                btn5.setChecked(true);
            }
            btnRatingDone.setImageResource(R.drawable.ic_done_all_black_24dp);
            isClickButtonRating = false;
        }

        btnRatingDone.setOnClickListener(v -> {
            if (isClickButtonRating) {

                if (isSelect) {
                    presenter.setRestaurantData(data);
                    presenter.setUserRestaurantSelect(selectData);
                    Log.d("MRX", "size-" + selectData.size());
                    isClickButtonRating = false;
                    Toast.makeText(getContext(), "edited rating", Toast.LENGTH_SHORT).show();
                } else {
                    presenter.setUserRestaurantSelect(selectData);
                    data.setCount((data.getCount() + 1));
                    data.setSumRating(data.getSumRating() + selectData.get(selectData.size() - 1).getRating());
                    data.setRating((data.getSumRating()) / data.getCount());
                    presenter.setRestaurantData(data);
                    btnRatingDone.setImageResource(R.drawable.ic_done_all_black_24dp);
                    isClickButtonRating = false;
                    clickRatingBtns();
                }
            }
        });
    }

    @Override
    protected boolean isToolbarShow() {
        return true;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return true;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    public static RestaurantInfoFragment getInstance(RestaurantData data) {
        RestaurantInfoFragment fragment = new RestaurantInfoFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA", data);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setData(List<SelectData> data) {
        this.selectData = data;
        if (open) {
            open = false;
            clickRatingBtns();
        }
    }

    @Override
    public void setRecentlyData(List<RecentlyViewedData> recentlyViewedData) {
        if (recentlyViewedData.size() == 10) {
            RecentlyViewedData viewedData = recentlyViewedData.get(recentlyViewedData.size() - 1);
            presenter.deleteRecentlyViewData(viewedData);
        }
        boolean isHave = false;
        for (int i = 0; i < recentlyViewedData.size(); i++) {
            RecentlyViewedData viewedData = recentlyViewedData.get(i);
            if (data.getName().equals(viewedData.getName())) {
                presenter.deleteRecentlyViewData(viewedData);
                recentlyViewedData.remove(viewedData);
                presenter.setRecentlyViewData(new RecentlyViewedData("restaurant", data.getCity(), data.getName(), data.getImgUrl(), recentlyViewedData.get(0).getPosition() + 1, data.getReview(), data.getRating(), 0));
                EventBus.getDefault().post(viewedData);
                isHave = true;
            }
        }

        if (!isHave) {
            RecentlyViewedData viewedData;
            if (recentlyViewedData != null && recentlyViewedData.size() > 0){
            viewedData = new RecentlyViewedData("restaurant", data.getCity(), data.getName(), data.getImgUrl(), recentlyViewedData.get(0).getPosition() + 1, data.getReview(), data.getRating(), 0);
            }
            else
            viewedData = new RecentlyViewedData("restaurant", data.getCity(), data.getName(), data.getImgUrl(),  1, data.getReview(), data.getRating(), 0);

            presenter.setRecentlyViewData(viewedData);
            EventBus.getDefault().post(viewedData);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked && !openSelect) {

            if (selectData == null)
                selectData = new ArrayList<>();
            if (isSelect) {
                Log.d("MRX", "select=" + isSelect);
                isSelectEvent(Integer.parseInt(buttonView.getText().toString()));
            } else {
                Log.d("MRX", "select=" + isSelect);
                isClickButtonRating = true;
                selectData.add(new SelectData(data.getName(), Integer.parseInt(buttonView.getText().toString())));

            }
        }
        openSelect = false;
    }

    private void isSelectEvent(int newRating) {
        data.setSumRating(data.getSumRating() - selectData.get(selectPosition).getRating() + newRating);
        data.setRating(data.getSumRating() / data.getCount());
        selectData.get(selectPosition).setRating(newRating);
        Log.d("MRX", "size-" + selectData.size());
        isClickButtonRating = true;
    }
}
