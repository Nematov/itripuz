package uz.N00003990.itripuz.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.custom_ui.CustomRatingBar;
import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.room.database.Database;

public class RecentlyViewedDataFragment extends BaseFragment {
    private RecentlyViewedData data;
    private ImageView img;
    private ImageView favorite;
    private ImageView imageView;
    private TextView name;
    private TextView reviews;
    private TextView textView;
    private CustomRatingBar ratingBar;
    private RadioGroup group;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recently_data_info, container, false);
        data = (RecentlyViewedData) (getArguments() != null ? getArguments().getSerializable("data") : null);
        findViewById(view);
        setToView();
        favorite.setOnClickListener(v -> {
            if (data.getFavorite() == 0) {
                favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                data.setFavorite(1);
                Toast.makeText(getContext(), "Added in favorite", Toast.LENGTH_SHORT).show();
                updateDataAtDatabase(data);
            } else {
                favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                data.setFavorite(0);
                Toast.makeText(getContext(), "Removed in favorite", Toast.LENGTH_SHORT).show();
                updateDataAtDatabase(data);
            }

        });
        return view;
    }

    private void findViewById(View view) {
        name = view.findViewById(R.id.info_recently_name);
        reviews = view.findViewById(R.id.info_recently_reviews);
        ratingBar = view.findViewById(R.id.rating);
        img = view.findViewById(R.id.app_bar_image);
        favorite = view.findViewById(R.id.recently_info_favorite);
        group = view.findViewById(R.id.group_rtn);
        group.setVisibility(View.INVISIBLE);
        textView = view.findViewById(R.id.set_rating);
        textView.setVisibility(View.INVISIBLE);
        imageView = view.findViewById(R.id.btn_rtn);
        imageView.setVisibility(View.INVISIBLE);
    }

    private void updateDataAtDatabase(RecentlyViewedData data) {
        Database.getDatabase().getRecentlyViewedDao().update(data);
    }

    @SuppressLint("SetTextI18n")
    private void setToView() {
        name.setText(data.getName());
        reviews.setText(data.getReviews() + " Reviews");
        ratingBar.setRating(5, data.getRating());
        ratingBar.setColor(ContextCompat.getColor(getContext(), R.color.colorRating));
        Picasso.get()
                .load(data.getImgUrl())
                .centerCrop()
                .fit()
                .into(img);
        if (data.getFavorite() == 1)
            favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
        else
            favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
    }

    public static Fragment getInstance(RecentlyViewedData data) {
        RecentlyViewedDataFragment fragment = new RecentlyViewedDataFragment();
        Bundle bundle = new Bundle();

        bundle.putSerializable("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected boolean isToolbarShow() {
        return true;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return true;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            getFragmentManager().popBackStack();
        }
        return true;
    }
}
