package uz.N00003990.itripuz.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.NotifManagerAdapter;
import uz.N00003990.itripuz.adapters.NotifUserAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.dialogs.LoadingDialog;
import uz.N00003990.itripuz.holders.NotifManagerHolder;
import uz.N00003990.itripuz.models.BusyData;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.NotificationModel;
import uz.N00003990.itripuz.settings.Settings;

public class NotifFragment extends BaseFragment {
    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private String uId;
    private ArrayList<NotificationModel> notificationModels;
    private NotifManagerAdapter notifManagerAdapter;
    private NotifUserAdapter notifUserAdapter;
    private LoadingDialog loadingDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        loadingDialog = new LoadingDialog(getContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        uId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        notificationModels = new ArrayList<>();
        if (Settings.getSettings().getBoolean("admin")) getManagerNotif();
        else getUserNotify();
    }

    private void getManagerNotif() {
        databaseReference.child("notifications")
                .orderByChild("managerId")
                .equalTo(uId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<NotificationModel> models = new ArrayList<>();
                        if (notificationModels.size()>0)
                            notificationModels.clear();
                        if (dataSnapshot.getValue() != null) {
                            Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();

//                            notificationModelMap = (Map<String, NotificationModel>) dataSnapshot.getValue();

                            for (DataSnapshot snapshot : iterable) {
                                NotificationModel notificationModel = snapshot.getValue(NotificationModel.class);
                                models.add(notificationModel);
                            }
                            for (int i = 0; i < models.size(); i++) {
                                if (models.get(i).getAccepted().equals("no")) {
                                    notificationModels.add(models.get(i));
                                }
                            }
                            notifManagerAdapter = new NotifManagerAdapter(notificationModels);
                            notifManagerAdapter.setOnClickNotifDataListener(new NotifManagerHolder.OnClickNotifDataListener() {
                                @Override
                                public void onClickOk(int p, NotificationModel notificationModel) {
                                    loadingDialog.show();
                                    notificationModels.remove(p);
                                    notifManagerAdapter.notifyItemRemoved(p);
                                    notificationModel.setAccepted("Yes");
                                    notificationModel.setManagerAnswered("Yes");
                                    databaseReference.child("notifications")
                                            .child(notificationModel.getKey())
                                            .setValue(notificationModel);
                                    databaseReference
                                            .child("cities")
                                            .child("toshkent")
                                            .child("hotels")
                                            .child(notificationModel.getHotelName())
                                            .child("rooms")
                                            .child(notificationModel.getRoomName())
                                            .child("busyData")
                                            .setValue(notificationModel.getBusyData())
                                            .addOnSuccessListener(command -> {
                                                loadingDialog.hide();
                                            });

                                }

                                @Override
                                public void onClickCancel(int p, NotificationModel notificationModel) {
                                    loadingDialog.show();
                                    notificationModels.remove(p);
                                    notifManagerAdapter.notifyItemRemoved(p);
                                    notificationModel.setAccepted("Cancel");
                                    notificationModel.setManagerAnswered("Cancel");
                                    databaseReference.child("notifications")
                                            .child(notificationModel.getKey())
                                            .setValue(notificationModel)
                                            .addOnSuccessListener(command -> {
                                                loadingDialog.hide();

                                            });
                                }
                            });
                            recyclerView.setAdapter(notifManagerAdapter);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }


    private void getUserNotify() {
        databaseReference.child("notifications")
                .orderByChild("userId")
                .equalTo(uId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (notificationModels.size() > 0)
                            notificationModels.clear();

                        if (dataSnapshot.getValue() != null) {
                            Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                            for (DataSnapshot snapshot : iterable) {
                                NotificationModel notificationModel = snapshot.getValue(NotificationModel.class);
                                notificationModels.add(notificationModel);
                            }
                            notifUserAdapter = new NotifUserAdapter(notificationModels);
                            recyclerView.setAdapter(notifUserAdapter);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    protected boolean isToolbarShow() {
        return !Settings.getSettings().getBoolean("admin");
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }
}
