package uz.N00003990.itripuz.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.MyTripPagerAdapter;
import uz.N00003990.itripuz.adapters.RecentlyDataAdapter;
import uz.N00003990.itripuz.core.BaseFragment;
import uz.N00003990.itripuz.holders.RecentlyDataHolder;
import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.mvp.presenter_implement.MyTripFragmentPresenterImpl;
import uz.N00003990.itripuz.mvp.views.MyTripFragmentView;

public class TripFragment extends BaseFragment implements MyTripFragmentView, RecentlyDataHolder.OnClickItem {
    private MyTripFragmentPresenterImpl presenter;

    private RecyclerView list;
    private RecentlyDataAdapter adapter;
    private MyTripPagerAdapter pagerAdapter;
    private List<RecentlyViewedData> recentlyViewedData;
    private String key;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_pager_my_trip, container, false);
        EventBus.getDefault().register(this);
        presenter = new MyTripFragmentPresenterImpl(this);
        list = view.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        assert getArguments() != null;
        key = getArguments().getString("key");
        presenter.loadRecentlyViewData(key);

        pagerAdapter = (MyTripPagerAdapter) getArguments().getSerializable("adapter");
        return view;
    }

    public static Fragment getInstance(String key, MyTripPagerAdapter adapter) {
        TripFragment fragment = new TripFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("adapter", adapter);
        bundle.putString("key", key);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected boolean isToolbarShow() {
        return false;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    public void setRecentlyDataToView(List<RecentlyViewedData> recentlyDataToView) {
        this.recentlyViewedData = recentlyDataToView;
        adapter = new RecentlyDataAdapter(recentlyViewedData);
        list.setAdapter(adapter);
        adapter.setOnClickFavoriteItem(this);
    }

    @Override
    public void onClickFavorite(RecentlyViewedData data, int position) {
        if (data.getFavorite() == 0){
            data.setFavorite(1);
        }
        else {
            data.setFavorite(0);
        }
        EventBus.getDefault().post(data);
    }

    @Override
    public void onClickItem(RecentlyViewedData data) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.layout,RecentlyViewedDataFragment.getInstance(data),"info");
        transaction.addToBackStack(null);
        transaction.commit();
    }



    @Subscribe
    public void notifyAdapter(RecentlyViewedData data) {
        if (key.equals("FAVORITE")) {
            if (data.getFavorite() == 1) {
                recentlyViewedData.add(data);
                adapter.notifyItemInserted(adapter.getItemCount() + 1);
            } else {
                int i = 0;
                while (i < recentlyViewedData.size()) {
                    if (recentlyViewedData.get(i).getName().equals(data.getName())) {
                        recentlyViewedData.remove(i);
                        adapter.notifyItemRemoved(i);
                        }
                    i++;
                }
            }
        } else {
             int i = 0;
                while (i<recentlyViewedData.size()){
                    if (recentlyViewedData.get(i).getName().equals(data.getName())){
                        presenter.updataData(data);
                        recentlyViewedData.get(i).setFavorite(data.getFavorite());
                        adapter.notifyItemChanged(i);
                    }
                    i++;
                }
//                presenter.updataData(data.getData());
//                adapter.notifyItemChanged(data.getPosition());
        }
    }
}
