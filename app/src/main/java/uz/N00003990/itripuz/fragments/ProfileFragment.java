package uz.N00003990.itripuz.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import uz.N00003990.itripuz.AdminActivity;
import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.core.BaseFragment;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment {
    private ConstraintLayout layout;
    private CircleImageView imageView;
    private TextView name;
    private TextView mail;
    private FirebaseUser user;
    private Button button;
    private Button signUp;

    private static final Integer RESULT_LOAD_IMAGE = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        findViewById(view);
        user = FirebaseAuth.getInstance().getCurrentUser();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (user != null) {
            updateUI();
        }
        signUp.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), AdminActivity.class));
        });
    }

    private void updateUI() {
        signUp.setVisibility(View.GONE);
        layout.setVisibility(View.VISIBLE);
        name.setText(user.getDisplayName());
        mail.setText(user.getEmail());
        if (user.getPhotoUrl() != null) {
            Picasso.get()
                    .load(user.getPhotoUrl())
                    .into(imageView);
        }

        button.setOnClickListener(this::getImageFromAlbum);
    }

    private void editPhoto() {

    }

    private void getImageFromAlbum(View view) {
        try {
            Intent i = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, RESULT_LOAD_IMAGE);
        } catch (Exception exp) {
            Log.i("Error", exp.toString());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = Objects.requireNonNull(getActivity()).getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

        }

    }

    private void findViewById(View view) {
        imageView = view.findViewById(R.id.profile_img);
        name = view.findViewById(R.id.profile_name);
        mail = view.findViewById(R.id.profile_last_name);
        button = view.findViewById(R.id.edit);
        signUp = view.findViewById(R.id.sign_up);
        layout = view.findViewById(R.id.profile_layout);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (user != null)
            inflater.inflate(R.menu.log_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.log_out) {
            FirebaseAuth.getInstance().signOut();
//            startActivity(new Intent(getContext(), LoginActivity.class));
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected boolean isToolbarShow() {
        return true;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }
}
