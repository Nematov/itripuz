package uz.N00003990.itripuz.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.adapters.MyTripPagerAdapter;
import uz.N00003990.itripuz.core.BaseFragment;

public class MyTripFragment extends BaseFragment {
    private ViewPager pager;
    private MyTripPagerAdapter pagerAdapter;
    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_trip, container, false);
        pager = view.findViewById(R.id.pager);
        tabLayout = view.findViewById(R.id.tabs);
        boolean aBoolean;
        aBoolean = getArguments() != null && getArguments().getBoolean("key");
        pagerAdapter = new MyTripPagerAdapter(getChildFragmentManager());
        pager.setAdapter(pagerAdapter);
        eventElement();
        tabLayout.setupWithViewPager(pager);
        if (aBoolean) {
            pager.setCurrentItem(1);
        }

        return view;
    }

    private void eventElement() {
        pager.addOnPageChangeListener((new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.setScrollPosition(position, 0, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        }));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public static Fragment getInstance(boolean b) {
        MyTripFragment fragment = new MyTripFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("key", b);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected boolean isToolbarShow() {
        return true;
    }

    @Override
    protected boolean isToolbarDisplayHomeShow() {
        return false;
    }

    @Override
    protected boolean isLoadingIndicatorUse() {
        return false;
    }

    @Override
    protected String getToolbarTitle() {
        return "My Trip";
    }
}
