package uz.N00003990.itripuz.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import uz.N00003990.itripuz.R;
import uz.N00003990.itripuz.models.CitiesData;


/**
 * Created by Sherzodbek on 28.02.2018 in Lesson25ICT1.
 */

@SuppressLint("ValidFragment")
public class ImagePagerFragment extends Fragment{
    private ImageView imageView;
    private FirebaseDatabase database;
    private DatabaseReference reference;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_pager,container,false);
        imageView = view.findViewById(R.id.pager_img);
        database = FirebaseDatabase.getInstance();
        String key = getArguments().getString("key");
        reference = database.getReference("cities").child(key);
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("NewApi")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                CitiesData citiesData = dataSnapshot.getValue(CitiesData.class);
                Picasso.get().load(citiesData.getImgUrl()).error(R.drawable.ic_error_black_24dp)
                        .into(imageView);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return view;
    }

    public static Fragment getInstance(String s){
        Fragment fragment = new ImagePagerFragment();
       Bundle bundle  = new Bundle();
       bundle.putString("key",s);
        fragment.setArguments(bundle);
        return fragment;
    }

}
