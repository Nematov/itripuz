package uz.N00003990.itripuz.mvp.presenter;

import uz.N00003990.itripuz.models.HotelData;

public interface AdminActivityPresenter {
    void getMyHotelList(String uId);

    void editMyHotelData(HotelData hotelData);

    void deleteMyHotelData(HotelData hotelData);
}
