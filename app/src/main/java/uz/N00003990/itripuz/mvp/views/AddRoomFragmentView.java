package uz.N00003990.itripuz.mvp.views;

public interface AddRoomFragmentView {
    void onSuccessRoom();
    void onErrorRoom();
}
