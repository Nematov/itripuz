package uz.N00003990.itripuz.mvp.views;

import java.util.List;

import uz.N00003990.itripuz.core.BaseView;
import uz.N00003990.itripuz.models.HotelData;

public interface HotelFragmentView extends BaseView{
    void getViewListHotelData(List<HotelData> data);
}
