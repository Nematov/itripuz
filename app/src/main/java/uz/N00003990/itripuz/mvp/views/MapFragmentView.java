package uz.N00003990.itripuz.mvp.views;

import java.util.List;

import uz.N00003990.itripuz.models.HotelData;

public interface MapFragmentView {

    void setDataToView(List<HotelData> data);
}
