package uz.N00003990.itripuz.mvp.views;

import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.SelectData;

public interface RestaurantInfoFragmentView {
    void setData(List<SelectData>  data);

    void setRecentlyData(List<RecentlyViewedData> recentlyViewedData);
}
