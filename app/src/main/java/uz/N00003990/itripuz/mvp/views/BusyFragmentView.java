package uz.N00003990.itripuz.mvp.views;

import java.util.ArrayList;

import uz.N00003990.itripuz.models.BusyData;

public interface BusyFragmentView {
    void setBusyList(ArrayList<BusyData> busyList);

    void onSendNotification();

    void onError();
}
