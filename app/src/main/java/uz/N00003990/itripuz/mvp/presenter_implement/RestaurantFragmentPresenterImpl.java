package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.models.RestaurantData;
import uz.N00003990.itripuz.mvp.presenter.RestaurentFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.RestaurantFragmentView;

public class RestaurantFragmentPresenterImpl implements RestaurentFragmentPresenter {
    private RestaurantFragmentView view;

    private FirebaseDatabase database;
    private DatabaseReference reference;
    private List<RestaurantData> data;

    public RestaurantFragmentPresenterImpl(RestaurantFragmentView view) {
        this.view = view;
        data = new ArrayList<>();
        database = FirebaseDatabase.getInstance();
        view.showLoading();
    }


    @Override
    public void loadData() {
        reference = database.getReference("cities").child("toshkent").child("Restaurants");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                if (data.size() > 0)
                    data.clear();

                for (DataSnapshot snapshot : iterable) {
                    RestaurantData restaurantData = snapshot.getValue(RestaurantData.class);
                    data.add(restaurantData);
                }

                view.setDataToView(data);
                view.hideLoading();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
