package uz.N00003990.itripuz.mvp.views;

import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;

public interface MyTripFragmentView {

    void setRecentlyDataToView(List<RecentlyViewedData> recentlyDataToView);
}
