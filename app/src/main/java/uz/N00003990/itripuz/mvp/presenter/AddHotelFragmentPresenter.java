package uz.N00003990.itripuz.mvp.presenter;

import android.net.Uri;

import uz.N00003990.itripuz.models.HotelData;

public interface AddHotelFragmentPresenter {
    void uploadImage(Uri uri);

    void saveDataToDatabase(HotelData hotelData);

    void getMyHotelList(String uId);
}
