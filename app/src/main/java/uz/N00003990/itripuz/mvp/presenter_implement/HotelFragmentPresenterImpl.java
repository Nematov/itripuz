package uz.N00003990.itripuz.mvp.presenter_implement;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.mvp.presenter.HotelFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.HotelFragmentView;

public  class HotelFragmentPresenterImpl implements HotelFragmentPresenter{
    private HotelFragmentView view;

    private FirebaseDatabase database;
    private DatabaseReference databaseReference;

    private List<HotelData> hotelLists;

    public HotelFragmentPresenterImpl(HotelFragmentView view) {
        this.view = view;
        view.showLoading();
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("cities").child("toshkent").child("hotels");
        hotelLists = new ArrayList<>();

    }


    @Override
    public void updateHotelData() {
        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                if (hotelLists.size() > 0)
                    hotelLists.clear();

                for (DataSnapshot snapshot : iterable) {
                    HotelData hotel = snapshot.getValue(HotelData.class);
                    hotelLists.add(hotel);
                }

                view.getViewListHotelData(hotelLists);

                view.hideLoading();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
