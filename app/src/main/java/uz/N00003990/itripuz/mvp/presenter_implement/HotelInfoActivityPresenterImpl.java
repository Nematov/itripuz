package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.SelectData;
import uz.N00003990.itripuz.models.UserRating;
import uz.N00003990.itripuz.mvp.presenter.HotelInfoActivityPresenter;
import uz.N00003990.itripuz.mvp.views.HotelInfoActivityView;
import uz.N00003990.itripuz.room.dao.RecentlyViewedDao;
import uz.N00003990.itripuz.room.database.Database;

public class HotelInfoActivityPresenterImpl implements HotelInfoActivityPresenter {
    private HotelInfoActivityView view;
    private DatabaseReference reference;
    private DatabaseReference hotelRf;
    private RecentlyViewedDao dao;
    private List<SelectData> selectData;
    private List<RecentlyViewedData> recentlyViewedData;

    public HotelInfoActivityPresenterImpl(HotelInfoActivityView view) {
        this.view = view;
        selectData = new ArrayList<>();
        recentlyViewedData = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference().child("user")
                .child(Objects.requireNonNull(FirebaseAuth.getInstance()
                        .getCurrentUser())
                        .getUid()).child("hotelRating");

        hotelRf = FirebaseDatabase.getInstance().getReference("cities").child("toshkent").child("hotels");
        dao = Database.getDatabase().getRecentlyViewedDao();
    }


    @Override
    public void loadDataRating() {


        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                Log.d("MRX", "user=" + dataSnapshot.getValue());
                if (selectData.size() > 0)
                    selectData.clear();

                for (DataSnapshot snapshot : iterable) {
                    SelectData data = snapshot.getValue(SelectData.class);
                    selectData.add(data);
                }

                view.loadDataToView(selectData);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void setUserRating(UserRating rating) {
        reference.setValue(rating);
    }

    @Override
    public void loadRecentlyViewData() {
        recentlyViewedData = dao.getViewedData();
        view.setRecentlyViewData(recentlyViewedData);
    }

    @Override
    public void setHotelData(HotelData data) {
        hotelRf.child(data.getName()).setValue(data);
    }

    @Override
    public void setUserSelectHotel(List<SelectData> hotelName) {
        reference.setValue(hotelName);
    }

    @Override
    public void setRecentlyViewData(RecentlyViewedData recentlyViewData) {
        dao.insertViewedData(recentlyViewData);
    }

    @Override
    public void deleteRecentlyViewData(RecentlyViewedData data) {
        dao.deleteViewedData(data);
    }

    @Override
    public void updateRecentlyViewedData(RecentlyViewedData viewedData) {
        dao.update(viewedData);
    }
}