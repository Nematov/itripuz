package uz.N00003990.itripuz.mvp.presenter_implement;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.mvp.presenter.MapFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.MapFragmentView;

public class MapFragmentPresenterImpl implements MapFragmentPresenter {
    private MapFragmentView  view;

    private FirebaseDatabase database;
    private DatabaseReference reference;

    private List<HotelData>  hotelLists;

    public MapFragmentPresenterImpl(MapFragmentView view) {
        this.view = view;
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("cities").child("toshkent").child("hotels");
        hotelLists = new ArrayList<>();

    }

    @Override
    public void loadData() {
        reference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                if (hotelLists.size() > 0)
                    hotelLists.clear();

                for (DataSnapshot snapshot : iterable) {
                    HotelData hotel = snapshot.getValue(HotelData.class);
                    hotelLists.add(hotel);
                }

                view.setDataToView(hotelLists);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
