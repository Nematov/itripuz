package uz.N00003990.itripuz.mvp.presenter;

public interface SearchFragmentPresenter {
    void loadData(String newText);
}
