package uz.N00003990.itripuz.mvp.presenter_implement;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.mvp.presenter.MyTripFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.MyTripFragmentView;
import uz.N00003990.itripuz.room.dao.RecentlyViewedDao;
import uz.N00003990.itripuz.room.database.Database;

public class MyTripFragmentPresenterImpl implements MyTripFragmentPresenter {
    private MyTripFragmentView view;
    private RecentlyViewedDao dao;
    private List<RecentlyViewedData> recentlyViewedData;

    public MyTripFragmentPresenterImpl(MyTripFragmentView view) {
        this.view = view;
        recentlyViewedData = new ArrayList<>();
        dao = Database.getDatabase().getRecentlyViewedDao();
    }

    @Override
    public void loadRecentlyViewData(String key) {
        if (key.equals("FAVORITE")){
            recentlyViewedData = dao.getFavoriteData();
        }
        else {
            recentlyViewedData = dao.getViewedData();
        }

        view.setRecentlyDataToView(recentlyViewedData);
    }

    @Override
    public void updataData(RecentlyViewedData data) {
        dao.update(data);
    }
}
