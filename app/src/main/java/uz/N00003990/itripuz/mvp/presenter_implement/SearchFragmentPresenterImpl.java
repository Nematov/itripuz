package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.mvp.presenter.SearchFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.SearchFragmentView;

public class SearchFragmentPresenterImpl implements SearchFragmentPresenter {

    private SearchFragmentView view;


    private FirebaseDatabase database;
    private Query queryRf;
    private DatabaseReference databaseReference;
    private List<HotelData> data;

    public SearchFragmentPresenterImpl(SearchFragmentView view) {
        this.view = view;
        data = new ArrayList<>();
    }

    @Override
    public void loadData(String newText) {
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("cities").child("toshkent").child("hotels");

        queryRf = database.getReference("cities").child("toshkent").child("hotels")
                .orderByChild("name")
                .startAt(newText)
                .endAt(newText + "\uf8ff");

        queryRf.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (data.size() > 0)
                    data.clear();
                if (dataSnapshot.getValue() != null) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();


                    for (DataSnapshot snapshot : iterable) {
                        HotelData hotel = snapshot.getValue(HotelData.class);
                        data.add(hotel);
                    }
                    view.setDataToView(data);

                } else {
                    view.noResult();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("MRX", "error=" + databaseError.getMessage());

            }
        });
    }
}
