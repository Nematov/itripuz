package uz.N00003990.itripuz.mvp.presenter;

import java.util.Map;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RoomData;

public interface AddRoomFragmentPresenter {
    void saveRoom(String hotel, Map<String,RoomData> roomData);

}
