package uz.N00003990.itripuz.mvp.views;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;

public interface ScrollingActivityView {

    void setDataToView(ArrayList<String> citiesList);

    void setRecentlyDataToView(List<RecentlyViewedData> recentlyDataToView);

    void showLoading();

    void hideLoading();
}
