package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.mvp.presenter.ScrollingActivityPresenter;
import uz.N00003990.itripuz.mvp.views.ScrollingActivityView;
import uz.N00003990.itripuz.room.dao.RecentlyViewedDao;
import uz.N00003990.itripuz.room.database.Database;

public class ScrollingActivityPresenterImple implements ScrollingActivityPresenter {
    private ScrollingActivityView view;

    private FirebaseDatabase database;
    private DatabaseReference reference;
    private RecentlyViewedDao dao;

    private ArrayList<String> citiesList;
    private List<RecentlyViewedData> recentlyViewedData;


    public ScrollingActivityPresenterImple(ScrollingActivityView view) {
        this.view = view;
        citiesList = new ArrayList<>();
        recentlyViewedData = new ArrayList<>();
        view.showLoading();
        database = FirebaseDatabase.getInstance();
        dao = Database.getDatabase().getRecentlyViewedDao();
    }


    @Override
    public void loadData() {
        reference = database.getReference("cities");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                if (citiesList.size() > 0)
                    citiesList.clear();

                for (DataSnapshot snapshot : iterable) {
                    String city = snapshot.getKey();
                    citiesList.add(city);
                }

                view.setDataToView(citiesList);
                view.hideLoading();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void loadRecentlyViewedData() {
        recentlyViewedData = dao.getViewedData();
        view.setRecentlyDataToView(recentlyViewedData);
    }
}
