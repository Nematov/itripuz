package uz.N00003990.itripuz.mvp.views;

import java.util.ArrayList;

import uz.N00003990.itripuz.models.HotelData;

public interface AddHotelFragmentView {
    void onSuccessUploadImage(String url);

    void onErrorUploadImage();

    void onSuccessSaveDataToDatabase();

    void onErrorSaveDataToDatabase();

    void setDataToView(ArrayList<HotelData> data);

    void noResult();
}
