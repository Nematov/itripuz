package uz.N00003990.itripuz.mvp.presenter_implement;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Objects;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.HotelList;
import uz.N00003990.itripuz.mvp.presenter.AddHotelFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.AddHotelFragmentView;

public class AddHotelPresenterImpl implements AddHotelFragmentPresenter {
    private AddHotelFragmentView view;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private Query query;
    private ArrayList<HotelData> hotelData;

    public AddHotelPresenterImpl(AddHotelFragmentView view) {
        this.view = view;
        storage = FirebaseStorage.getInstance();
        hotelData = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference("cities").child("toshkent").child("hotels");

    }

    @Override
    public void uploadImage(Uri uri) {
        storageReference = storage.getReference().child("images/" + uri.getPath());
        UploadTask uploadTask = storageReference.putFile(uri);
        uploadTask.
                continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference.getDownloadUrl();
                }).
                addOnSuccessListener(command -> {
                    view.onSuccessUploadImage(command.toString());
                }).addOnFailureListener(command -> {
        });
    }

    @Override
    public void saveDataToDatabase(HotelData hotelData) {
        databaseReference.child(hotelData.getName())
                .setValue(hotelData)
        .addOnSuccessListener(command -> {
            view.onSuccessSaveDataToDatabase();
        });
    }

    @Override
    public void getMyHotelList(String uId) {
        query = databaseReference
                .orderByChild("uId").equalTo(uId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (hotelData.size() > 0)
                    hotelData.clear();
                if (dataSnapshot.getValue() != null) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    for (DataSnapshot snapshot : iterable) {
                        HotelData hotel = snapshot.getValue(HotelData.class);
                        hotelData.add(hotel);
                    }
                    view.setDataToView(hotelData);
                } else {
                    view.noResult();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
