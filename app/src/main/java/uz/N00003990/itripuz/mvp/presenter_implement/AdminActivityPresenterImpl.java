package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import uz.N00003990.itripuz.adapters.HotelAdapter;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.mvp.presenter.AdminActivityPresenter;
import uz.N00003990.itripuz.mvp.views.AdminActivityView;

public class AdminActivityPresenterImpl implements AdminActivityPresenter {
    private AdminActivityView view;
    private DatabaseReference databaseReference;
    private Query query;
    private ArrayList<HotelData> hotelData;

    public AdminActivityPresenterImpl(AdminActivityView view) {
        this.view = view;
        hotelData = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference("cities").child("toshkent").child("hotels");

    }

    @Override
    public void getMyHotelList(String uId) {
        query = databaseReference
                .orderByChild("uId").equalTo(uId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (hotelData.size() > 0)
                    hotelData.clear();
                if (dataSnapshot.getValue() != null) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();


                    for (DataSnapshot snapshot : iterable) {
                        HotelData hotel = snapshot.getValue(HotelData.class);
                        hotelData.add(hotel);
                    }
                    view.onSetMyHotelData(hotelData);
                } else {
                    //empty
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void editMyHotelData(HotelData hotelData) {

    }

    @Override
    public void deleteMyHotelData(HotelData hotelData) {
        databaseReference.child(hotelData.getName())
                .removeValue()
                .addOnSuccessListener(command -> {
                    view.onSuccessDelete();
                });
    }
}
