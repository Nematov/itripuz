package uz.N00003990.itripuz.mvp.presenter_implement;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Map;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RoomData;
import uz.N00003990.itripuz.mvp.presenter.AddRoomFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.AddRoomFragmentView;

public class AddRoomPresenterImpl implements AddRoomFragmentPresenter {
    private AddRoomFragmentView view;
    private DatabaseReference databaseReference;

    public AddRoomPresenterImpl(AddRoomFragmentView view) {
        this.view = view;
        databaseReference = FirebaseDatabase.getInstance().getReference("cities").child("toshkent").child("hotels");
    }

    @Override
    public void saveRoom(String hotel, Map<String,RoomData> roomData) {
        databaseReference.child(hotel).child("rooms")
//                .child(roomData.getName())
                .setValue(roomData);
        view.onSuccessRoom();
    }
}
