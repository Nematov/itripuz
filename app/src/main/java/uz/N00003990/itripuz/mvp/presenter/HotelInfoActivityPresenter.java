package uz.N00003990.itripuz.mvp.presenter;

import java.util.List;

import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.SelectData;
import uz.N00003990.itripuz.models.UserRating;

public interface HotelInfoActivityPresenter {

    void loadDataRating();

    void setUserRating(UserRating rating);

    void loadRecentlyViewData();

    void setHotelData(HotelData data);

    void setUserSelectHotel(List<SelectData> hotelName);

    void setRecentlyViewData(RecentlyViewedData recentlyViewData);

    void deleteRecentlyViewData(RecentlyViewedData data);

    void updateRecentlyViewedData(RecentlyViewedData viewedData);
}

