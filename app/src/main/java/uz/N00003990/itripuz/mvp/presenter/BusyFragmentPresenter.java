package uz.N00003990.itripuz.mvp.presenter;

import java.util.ArrayList;

import uz.N00003990.itripuz.models.BusyData;
import uz.N00003990.itripuz.retrofit.models.MessageData;

public interface BusyFragmentPresenter {
    void getList(String hotelName,String roomName,String date);

    void setBusy(ArrayList<BusyData> busy, String hotelName, String roomName);

    void sendNotification(MessageData messageData);
}
