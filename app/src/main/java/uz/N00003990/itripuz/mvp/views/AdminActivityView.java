package uz.N00003990.itripuz.mvp.views;

import java.util.ArrayList;

import uz.N00003990.itripuz.models.HotelData;

public interface AdminActivityView {
    void onSetMyHotelData(ArrayList<HotelData> hotelData);

    void onSuccessEdit();

    void onSuccessDelete();

    void onErrorEdit();

    void onErrorDelete();
}
