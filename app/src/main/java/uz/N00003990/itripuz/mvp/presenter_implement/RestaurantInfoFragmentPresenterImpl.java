package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.RestaurantData;
import uz.N00003990.itripuz.models.SelectData;
import uz.N00003990.itripuz.mvp.presenter.RestaurantInfoFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.RestaurantInfoFragmentView;
import uz.N00003990.itripuz.room.dao.RecentlyViewedDao;
import uz.N00003990.itripuz.room.database.Database;

public class RestaurantInfoFragmentPresenterImpl implements RestaurantInfoFragmentPresenter {
    private RestaurantInfoFragmentView view;
    private List<SelectData> data;
    private List<RecentlyViewedData> recentlyViewedData;
    private DatabaseReference reference;
    private DatabaseReference restaurantRf;
    private RecentlyViewedDao dao;

    public RestaurantInfoFragmentPresenterImpl(RestaurantInfoFragmentView view) {
        this.view = view;
        data = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference().child("user")
                .child(Objects.requireNonNull(FirebaseAuth.getInstance()
                        .getCurrentUser())
                        .getUid()).child("restaurantRating");
        restaurantRf = FirebaseDatabase.getInstance().getReference("cities").child("toshkent").child("Restaurants");
        dao = Database.getDatabase().getRecentlyViewedDao();
    }

    @Override
    public void loadData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                if (data.size() > 0)
                    data.clear();

                for (DataSnapshot snapshot : iterable) {
                    SelectData selectData = snapshot.getValue(SelectData.class);
                    data.add(selectData);
                }

                view.setData(data);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void setRestaurantData(RestaurantData data) {
        restaurantRf.child(data.getName()).setValue(data);
    }

    @Override
    public void setUserRestaurantSelect(List<SelectData> selectData) {
        reference.setValue(selectData);
    }

    @Override
    public void loadRecentlyData() {
        recentlyViewedData = dao.getViewedData();
        view.setRecentlyData(recentlyViewedData);
    }

    @Override
    public void deleteRecentlyViewData(RecentlyViewedData viewedData) {
        dao.deleteViewedData(viewedData);
    }

    @Override
    public void setRecentlyViewData(RecentlyViewedData recentlyViewData) {
        dao.insertViewedData(recentlyViewData);
    }
}
