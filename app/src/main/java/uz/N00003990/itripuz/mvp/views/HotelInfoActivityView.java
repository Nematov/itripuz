package uz.N00003990.itripuz.mvp.views;

import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.SelectData;

public interface HotelInfoActivityView {

    void loadDataToView(List<SelectData> selectData);

    void setRecentlyViewData(List<RecentlyViewedData> recentlyViewData);
}
