package uz.N00003990.itripuz.mvp.presenter;

import uz.N00003990.itripuz.models.RecentlyViewedData;

public interface MyTripFragmentPresenter {
    void loadRecentlyViewData(String key);

    void updataData(RecentlyViewedData data);
}
