package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import uz.N00003990.itripuz.models.RestaurantData;
import uz.N00003990.itripuz.models.VacationData;
import uz.N00003990.itripuz.mvp.presenter.VacationPresenter;
import uz.N00003990.itripuz.mvp.views.VacationView;

public class VacationPresenterImpl implements VacationPresenter {
    private VacationView vacationView;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private ArrayList<VacationData> vacationData;

    public VacationPresenterImpl(VacationView vacationView) {
        this.vacationView = vacationView;
        reference = FirebaseDatabase.getInstance().getReference("cities").child("toshkent").child("Vacations");
        vacationData = new ArrayList<>();
    }

    @Override
    public void loadData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                if (vacationData.size() > 0)
                    vacationData.clear();

                for (DataSnapshot snapshot : iterable) {
                    VacationData restaurantData = snapshot.getValue(VacationData.class);
                    vacationData.add(restaurantData);
                }
                vacationView.setData(vacationData);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
