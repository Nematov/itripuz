package uz.N00003990.itripuz.mvp.views;

import java.util.ArrayList;

import uz.N00003990.itripuz.models.VacationData;

public interface VacationView {
    public void setData(ArrayList<VacationData> data);
}
