package uz.N00003990.itripuz.mvp.views;

import java.util.List;

import uz.N00003990.itripuz.core.BaseView;
import uz.N00003990.itripuz.models.RestaurantData;

public interface RestaurantFragmentView extends BaseView{
    void setDataToView(List<RestaurantData> data);
}
