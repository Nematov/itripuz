package uz.N00003990.itripuz.mvp.presenter;

public interface ScrollingActivityPresenter {

    void loadData();

    void loadRecentlyViewedData();
}
