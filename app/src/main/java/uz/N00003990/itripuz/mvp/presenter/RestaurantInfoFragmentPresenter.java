package uz.N00003990.itripuz.mvp.presenter;

import java.util.List;

import uz.N00003990.itripuz.models.RecentlyViewedData;
import uz.N00003990.itripuz.models.RestaurantData;
import uz.N00003990.itripuz.models.SelectData;

public interface RestaurantInfoFragmentPresenter {

    void loadData();

    void setRestaurantData(RestaurantData  data);

    void setUserRestaurantSelect(List<SelectData> selectData);

    void loadRecentlyData();

    void deleteRecentlyViewData(RecentlyViewedData viewedData);

    void setRecentlyViewData(RecentlyViewedData recentlyViewData);
}
