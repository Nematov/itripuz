package uz.N00003990.itripuz.mvp.presenter_implement;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.N00003990.itripuz.models.BusyData;
import uz.N00003990.itripuz.models.HotelData;
import uz.N00003990.itripuz.mvp.presenter.BusyFragmentPresenter;
import uz.N00003990.itripuz.mvp.views.BusyFragmentView;
import uz.N00003990.itripuz.retrofit.ApiInterface;
import uz.N00003990.itripuz.retrofit.ApiRetrofit;
import uz.N00003990.itripuz.retrofit.models.MessageData;
import uz.N00003990.itripuz.retrofit.models.ResponseData;

public class BusyPresenterImpl implements BusyFragmentPresenter {
    private BusyFragmentView view;
    private DatabaseReference databaseReference;
    private Query query;
    private ArrayList<BusyData> busyData;
    private ApiInterface apiInterface;

    public BusyPresenterImpl(BusyFragmentView view) {
        this.view = view;
        databaseReference = FirebaseDatabase.getInstance().getReference("cities").child("toshkent").child("hotels");
        busyData = new ArrayList<>();
        apiInterface = ApiRetrofit.getRetrofit().create(ApiInterface.class);
    }

    @Override
    public void getList(String hotelName, String roomName, String date) {
        query = databaseReference.child(hotelName).child("rooms").child(roomName).child("busyData")
                .orderByChild("date").equalTo(date);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (busyData.size() > 0) busyData.clear();
                if (dataSnapshot.getValue() != null) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();


                    for (DataSnapshot snapshot : iterable) {
                        BusyData value = snapshot.getValue(BusyData.class);
                        busyData.add(value);
                    }
                    view.setBusyList(busyData);
                } else {
                    view.setBusyList(null);
                    //empty
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void setBusy(ArrayList<BusyData> busy, String hotelName, String roomName) {
        databaseReference.child(hotelName).child("rooms").child(roomName).child("busyData").setValue(busy);
    }

    @Override
    public void sendNotification(MessageData messageData) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("notifications");
        String k = databaseReference.push().getKey();
        messageData.getNotificationModel().setKey(k);
        databaseReference.child(k).setValue(messageData.getNotificationModel());
        String jsonObject = new Gson().toJson(messageData);
        Call<ResponseData> responseDataCall = apiInterface.sendMessage(new JsonParser().parse(jsonObject).getAsJsonObject());
        responseDataCall.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
               view.onSendNotification();
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Log.d("MRX", t.getMessage());
               view.onError();
            }
        });
    }
}
