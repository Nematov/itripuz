package uz.N00003990.itripuz;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class SignInActivity extends AppCompatActivity {
    private TextInputEditText phoneNumber;
    private TextInputEditText code;
    private Button nextButton;
    private Button signInButton;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        phoneNumber = findViewById(R.id.phone_number);
        code = findViewById(R.id.verification_code);
        nextButton = findViewById(R.id.next);
        signInButton = findViewById(R.id.sign_in_button);
        mAuth = FirebaseAuth.getInstance();
        initCallbacks();
        nextButton.setOnClickListener(this::onClickNext);
        signInButton.setOnClickListener(this::onClickSignIn);
    }

    private void onClickNext(View view) {
        String s = phoneNumber.getText().toString();
        if (isValidatePhoneNumber(s)) {
            sendCodetoNumber(s);
            nextButton.setVisibility(View.GONE);
            signInButton.setVisibility(View.VISIBLE);
            phoneNumber.setVisibility(View.GONE);
            code.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null){
            startActivity(new Intent(this, ScrollingActivity.class));
            finish();
        }
    }

    private void onClickSignIn(View view) {
        if (code.length() > 0) {
            signInWithPhoneAuthCredential(getCredential(code.getText().toString()));
        } else code.setError("write code");
    }

    private Boolean isValidatePhoneNumber(String number) {
        if (TextUtils.isEmpty(number) || number.length() != 13) {
            phoneNumber.setError("Invalid phone number");
            return false;
        }
        return true;
    }

    private void initCallbacks() {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d("MRX", "onVerificationCompleted:" + credential);

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w("MRX", "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    Toast.makeText(SignInActivity.this, "error code", Toast.LENGTH_SHORT).show();
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Toast.makeText(SignInActivity.this, "too many try please try again later",
                            Toast.LENGTH_SHORT).show();

                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d("MRX", "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // ...
            }
        };
    }

    private PhoneAuthCredential getCredential(String code) {
        return PhoneAuthProvider.getCredential(mVerificationId, code);
    }

    private void sendCodetoNumber(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("MRX", "signInWithCredential:success");

                        FirebaseUser user = task.getResult().getUser();
                        startActivity(new Intent(this, ScrollingActivity.class));
                        finish();
                    } else {
                        // Sign in failed, display a message and update the UI
                        Log.w("MRX", "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                        }
                    }
                });
    }
}
