package uz.N00003990.itripuz;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import java.util.Objects;

import uz.N00003990.itripuz.fragments.RestaurantInfoFragment;
import uz.N00003990.itripuz.models.RestaurantData;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Bundle bundle = Objects.requireNonNull(getIntent().getExtras()).getBundle("DATA");
        assert bundle != null;
       RestaurantData data = (RestaurantData) bundle.getSerializable("DATA");
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.info_activity, RestaurantInfoFragment.getInstance(data));
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count == 0) {
                Log.d("MRX","super");
                finish();
                //additional code
            } else {
                Log.d("MRX","stack");
                getSupportFragmentManager().popBackStack();
            }}
            return true;
    }
}
