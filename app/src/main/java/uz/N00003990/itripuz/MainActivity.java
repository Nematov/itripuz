package uz.N00003990.itripuz;

import android.annotation.SuppressLint;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;

import uz.N00003990.itripuz.fragments.HotelFragment;
import uz.N00003990.itripuz.fragments.RestaurantFragment;
import uz.N00003990.itripuz.fragments.VacationFragment;
import uz.N00003990.itripuz.settings.Settings;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MRX";
    private FragmentManager manager;
    private FragmentTransaction transaction;
    private SearchView search;


    @SuppressLint({"CommitTransaction", "ResourceAsColor"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();

        Bundle bundle = getIntent().getExtras();

        String key = Settings.getSettings().getKey("KEY");

        if (key.equals("hotel")){
            HotelFragment fragment = new HotelFragment();
            transaction.add(R.id.fragment_activity_layout,fragment,key);

            transaction.commit();
        }

        if (key.equals("restaurant")){
            RestaurantFragment fragment = new RestaurantFragment();
            transaction.add(R.id.fragment_activity_layout,fragment,key);
            transaction.commit();
        }
        if (key.equals("vacation")){
            VacationFragment fragment = new VacationFragment();
            transaction.add(R.id.fragment_activity_layout,fragment,key);
            transaction.commit();
        }


    }

    @Override
    public void onBackPressed() {
        int count = manager.getBackStackEntryCount();
        if (count == 0) {
            Log.d("MRX","super");
            super.onBackPressed();
            //additional code
        } else {
            Log.d("MRX","stack");
            manager.popBackStack();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            int count = manager.getBackStackEntryCount();
            if (count == 0) {
                Log.d("MRX","super");
               finish();
                //additional code
            } else {
                Log.d("MRX","stack");
                manager.popBackStack();
        }}
        return true;
    }
}
