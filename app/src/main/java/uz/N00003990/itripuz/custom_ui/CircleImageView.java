package uz.N00003990.itripuz.custom_ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import uz.N00003990.itripuz.R;


/**
 * TODO: document your custom view class.
 */
public class CircleImageView extends android.support.v7.widget.AppCompatImageView {
    private Drawable mCircleDrawable;

    public CircleImageView(Context context) {
        super(context);
        init(null, 0);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.CircleImageView, defStyle, 0);

        if (a.hasValue(R.styleable.CircleImageView_circleImage)) {
            mCircleDrawable = a.getDrawable(R.styleable.CircleImageView_circleImage);
            setImageDrawable(getCircleDrawable(mCircleDrawable));
        }
        a.recycle();
    }


    private Drawable getCircleDrawable(Drawable drawable) {
        Bitmap bitmap1 = ((BitmapDrawable) drawable).getBitmap();
        Bitmap bitmap = bitmap1.copy(bitmap1.getConfig(),true);
        int a = bitmap.getWidth() / 2;
        int b = bitmap.getHeight() / 2;
        int r = Math.min(a, b);
        for (int i = 0; i < bitmap.getWidth(); i++) {
            for (int j = 0; j < bitmap.getHeight(); j++) {
                if (getSquare(a - i) + getSquare(b - j) > r * r) {
                    bitmap.setPixel(i, j, Color.TRANSPARENT);
                }
            }
        }
        return new BitmapDrawable(getResources(), bitmap);
    }


    private int getSquare(int a) {
        return a * a;
    }

    public void setmCircleDrawable(Drawable mCircleDrawable) {
        this.mCircleDrawable = mCircleDrawable;
    }
}
