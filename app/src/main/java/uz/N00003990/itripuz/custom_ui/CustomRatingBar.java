package uz.N00003990.itripuz.custom_ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import uz.N00003990.itripuz.R;

public class CustomRatingBar extends View {
    private int maxRating = 0;
    private int rating = 0;
    private int radius = 50;
    private int strokeWidht = 3;
    private int padding = 5;
    @ColorInt private int colorId;


    public CustomRatingBar(Context context) {
        super(context);
    }

    public CustomRatingBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        @SuppressLint("Recycle") TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomRatingBar);
        radius = a.getInt(R.styleable.CustomRatingBar_radius,50);
    }

    public CustomRatingBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomRatingBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        init(canvas);
    }

    @SuppressLint("ResourceAsColor")
    private void init(Canvas canvas) {
        if (maxRating != 0 && maxRating > 0 && rating >= 0
                && maxRating >= rating) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(colorId);
            paint.setStrokeWidth(strokeWidht);
            Paint sP = new Paint(Paint.ANTI_ALIAS_FLAG);
            sP.setColor(Color.WHITE);
            int rS = radius - 5;
            float radiusSmall = radius * 0.6f;
            sP.setStrokeWidth(strokeWidht);
            for (int i = 0; i < maxRating; i++) {
                if (i < rating) {
                    canvas.drawCircle(i * (2 * radius + padding) + radius, radius, radius, paint);
                    canvas.drawCircle(i * (2 * radius + padding) + radius, radius, rS, sP);
                    canvas.drawCircle(i * (2 * radius + padding) + radius, radius, radiusSmall, paint);
                } else {

                    canvas.drawCircle(i * (2 * radius + padding) + radius, radius, radius, paint);
                    canvas.drawCircle(i * (2 * radius + padding) + radius, radius, rS, sP);
                    canvas.drawCircle(i * (2 * radius + padding) + radius, radius, radiusSmall, sP);
                }
            }
        }
    }

    public void setRating(int maxRating, int rating) {
        this.maxRating = maxRating;
        this.rating = rating;
        postInvalidate();
    }
    public void setColor(@ColorInt int colorId){
       this.colorId = colorId;
        postInvalidate();
    }
}
