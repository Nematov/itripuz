package uz.N00003990.itripuz.core;

public interface BaseView {
    void showLoading();
    void hideLoading();
}
