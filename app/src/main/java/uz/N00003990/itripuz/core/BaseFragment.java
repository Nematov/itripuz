package uz.N00003990.itripuz.core;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.Objects;

import uz.N00003990.itripuz.R;

public abstract class BaseFragment extends Fragment implements BaseView {
    private Toolbar toolbar;
    private AVLoadingIndicatorView loadingIndicatorView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (isToolbarShow())
            if (view.findViewById(R.id.toolbar) != null) {
                if (view.findViewById(R.id.toolbar) instanceof Toolbar) {
                    toolbar = view.findViewById(R.id.toolbar);
                    toolbar.setVisibility(View.VISIBLE);
                    initToolbar();
                }
            }

        if (isLoadingIndicatorUse())
            if (view.findViewById(R.id.loaderIndicator) != null) {
                if (view.findViewById(R.id.loaderIndicator) instanceof AVLoadingIndicatorView) {
                    loadingIndicatorView = view.findViewById(R.id.loaderIndicator);
                }
            }
    }

    private void initToolbar() {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(isToolbarDisplayHomeShow());
        toolbar.setSubtitleTextColor(ContextCompat.getColor(getContext(), getToolbarColor()));
        toolbar.setOnMenuItemClickListener(this::onOptionsItemSelected);
        toolbar.setTitle(getToolbarTitle());
        toolbar.setTitleTextColor(ContextCompat.getColor(getContext(), getToolbarColor()));
    }

    protected abstract boolean isToolbarShow();

    protected abstract boolean isToolbarDisplayHomeShow();

    protected abstract boolean isLoadingIndicatorUse();

    protected int getToolbarColor() {
        return R.color.colorWhite;
    }

    protected String getToolbarTitle() {
        return "";
    }

    @Override
    public void showLoading() {
        if (loadingIndicatorView != null)
            loadingIndicatorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        if (loadingIndicatorView != null) {
            loadingIndicatorView.setVisibility(View.INVISIBLE);
        }
    }
}
